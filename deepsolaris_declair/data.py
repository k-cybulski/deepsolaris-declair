import os
import random

import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset
from torchvision.transforms import ToTensor, Normalize
from torchvision.transforms import functional as F
from PIL.Image import open as open_image

from declair.frameworks.ignite.data import get_split_loaders as declair_get_split_loaders
from .transforms import standard_norm

DEFAULT_NUMPY_LABELS_ENDING = '_labels.npy'
DEFAULT_NUMPY_IMAGES_ENDING = '.npy'

class InvalidDatasetException(Exception):
    """Raised when an ambiguity or error occurs while loading a dataset."""

class InvalidTransformsException(Exception):
    """Raised when transforms performed on an image are invalid."""

class _DeepSolarisDataset(Dataset):
    # Abstract dataset for a bunch of repeating elements
    def __init__(self, name, data_dir, transform, base_transform, normalize, device, max_size):
        self._name = name
        self._data_dir = data_dir
        self._transform = transform
        self._base_transform = base_transform
        self._norm = (standard_norm() if normalize else None)
        self._device = device
        self._max_size = max_size

    def _size(self):
        raise NotImplementedError("This method needs to be overriden by children")

    def __len__(self):
        if self._max_size is None:
            return self._size()
        else:
            return min(self._max_size, self._size())

    def _get_image_label(self, idx):
        raise NotImplementedError("This method needs to be overriden by children")

    def _transform_image(self, image):
        # Turns whatever we start with into a tensor
        image = self._base_transform(image) 
        if self._transform:
            image = self._transform(image)
        if not isinstance(image, torch.Tensor):
            raise InvalidTransformsException("Dataset transforms must result in a Torch tensor")
        if self._norm:
            image = self._norm(image)
        return image

    def __getitem__(self, idx):
        image, label = self._get_image_label(idx)
        image = self._transform_image(image).to(self._device)
        label = torch.tensor(label, dtype=torch.long, device=self._device)
        return image, label

def _load_dataset_numpy(name, data_dir, 
                 images_file_ending=DEFAULT_NUMPY_IMAGES_ENDING,
                 labels_file_ending=DEFAULT_NUMPY_LABELS_ENDING):
    """Loads images and labels of a dataset."""
    images = np.load(os.path.join(data_dir, "{}{}".format(name, images_file_ending)))
    labels = np.load(os.path.join(data_dir, "{}{}".format(name, labels_file_ending)))
    return images, labels

class DeepSolarisDatasetNumpy(_DeepSolarisDataset):
    """A dataset of DeepSolaris images from numpy formatted images and labels.

    Images are augmented (transformed) as they are accessed. This makes
    training bottlenecked by CPU power.
    """
    def __init__(self, name, data_dir, transform=None, normalize=True, device='cpu', max_size=None):
        super().__init__(name, data_dir, transform, ToTensor(), normalize, device, max_size)
        self._images, self._labels = _load_dataset_numpy(name, data_dir)

    def _size(self):
        return len(self._images)

    def _get_image_label(self, idx):
        return self._images[idx], self._labels[idx]

# this was a helper function for when there was only one Dataset class
# now it's quite unnecessary clutter
def get_split_loaders(**kwargs):
    return declair_get_split_loaders(DeepSolarisDatasetNumpy, **kwargs)

def _find_image_files(name, root_data_dir, image_extension, path_extra='full'):
    if path_extra is not None:
        main_path = os.path.join(root_data_dir, name, path_extra)
    else:
        main_path = os.path.join(root_data_dir, name)
    filenames = list(filter(lambda x: x.endswith(image_extension),
                     os.listdir(main_path)))
    return filenames

def _get_path_with_extra(root_data_dir, name, path_extra):
    if path_extra is not None:
        main_path = os.path.join(root_data_dir, name, path_extra)
    else:
        main_path = os.path.join(root_data_dir, name)
    return main_path

def _find_csv_path(csv_name, main_path, dataset_name):
    if csv_name is None:
        csv_candidate_path = os.path.join(main_path, '{}.csv'.format(dataset_name))
        if os.path.isfile(csv_candidate_path):
            csv_path = csv_candidate_path
        else:
            csvs = list(filter(lambda x: x.endswith('.csv'),
                    os.listdir(main_path)))
            if len(csvs) == 0:
                raise InvalidDatasetException("No csv files in data directory")
            elif len(csvs) > 1:
                raise InvalidDatasetException("Too many candidate csv files for labels in data directory. Use csv_name to specify which file to use")
            else:
                csv_path = os.path.join(main_path, csvs[0])
    elif not os.path.isfile(os.path.join(main_path, csv_name)):
        raise InvalidDatasetException("CSV file {} does not exist".format(
            os.path.isfile(os.path.join(main_path, csv_name))))
    else:
        csv_path = os.path.join(main_path, csv_name)
    return csv_path

def _find_labels_from_csv(name, root_data_dir, csv_name, path_extra='full',
                 csv_delimiter=';', csv_filename_header='filename',
                 csv_label_header='label'):
    main_path = _get_path_with_extra(root_data_dir, name, path_extra)
    csv_path = _find_csv_path(csv_name, main_path, name)
    df = pd.read_csv(csv_path, delimiter=csv_delimiter)
    label_mapping = dict(zip(df[csv_filename_header], df[csv_label_header]))
    return label_mapping

def _filter_files_with_labels(filenames, label_mapping):
    return list(filter(lambda x: x in label_mapping, filenames))

class DeepSolarisDatasetFilesCSV(_DeepSolarisDataset):
    """A dataset of images from a directory with a csv file of labels.

    Images are expected to be of form
        <root_data_dir>/<name>/<path_extra>/*<image_extension>

    The default `path_extra` is `full`, as in main datasets used in
    DeepSolaris.

    If `csv_name` is set to None, then the csv is expected to be
        <root_data_dir>/<name>/<path_extra>/<name>.csv
    if such a file does not exist, but there is only a single csv in the `full`
    directory, then this single csv will be taken. If either none or more than
    one csv files exist, then InvalidDatasetException is raised.

    Otherwise, if `csv_name` is given, the csv is expected to be
        <root_data_dir>/<name>/<path_extra>/<csv_name>.csv
    """

    def __init__(self, name, root_data_dir, transform=None, normalize=True,
                 device='cpu', image_extension='.tiff', csv_name=None,
                 path_extra='full', csv_delimiter=';',
                 csv_filename_header='filename', csv_label_header='label',
                 shuffle_seed=42, max_size=None):
        super().__init__(name, root_data_dir, transform, ToTensor(), normalize, device, max_size)

        if not os.path.exists(os.path.join(root_data_dir, name)):
            raise InvalidDatasetException("Dataset {} does not exist in {}".format(name, root_data_dir))

        label_mapping = _find_labels_from_csv(
            name, root_data_dir, csv_name, path_extra=path_extra,
            csv_delimiter=csv_delimiter, csv_filename_header=csv_filename_header,
            csv_label_header=csv_label_header
        )
        # Sort to ensure reproducibility between machines
        filenames_of_interest = sorted(_filter_files_with_labels(
            _find_image_files(name, root_data_dir, image_extension=image_extension),
            label_mapping))
        # Shuffle to ensure that subsets of the dataset are random
        random.seed(shuffle_seed)
        random.shuffle(filenames_of_interest)

        main_path = _get_path_with_extra(root_data_dir, name, path_extra)
        self._images = [os.path.join(main_path, filename)
                        for filename in filenames_of_interest]
        self._labels = [label_mapping[filename] for filename in filenames_of_interest]

    def _size(self):
        return len(self._images)

    def _get_image_label(self, idx):
        image = open_image(self._images[idx]).convert('RGB')
        label = self._labels[idx]
        return image, label

def _find_class_names_from_dir(root_data_dir, name):
    path = os.path.join(root_data_dir, name)
    return [x.name for x in os.scandir(path) if x.is_dir()]

def _find_images_labels_from_nested_dir(root_data_dir, name, image_extension, classes, shuffle_seed):
    if classes is None:
        # Note that, alphabetically, "negative" < "positive" and thus by
        # sorting the class names we get 0 for negative and 1 for positive
        classes = sorted(_find_class_names_from_dir(root_data_dir, name))
    else:
        for class_ in classes:
            path = os.path.join(root_data_dir, name, class_)
            if not os.path.isdir(path):
                raise InvalidDatasetException("{} is not a directory".format(
                    path))
    images = []
    labels = []
    for idx, class_ in enumerate(classes):
        path = os.path.join(root_data_dir, name, class_)
        file_paths_of_class = [
            x.path for x in os.scandir(path) if (x.is_file() and x.name.endswith(image_extension))
        ]
        images += file_paths_of_class
        labels += [idx] * len(file_paths_of_class)
    # Shuffle so that, in the event only a subset of the dataset is used, this
    # subset is random
    zipped = list(zip(images, labels))
    random.seed(shuffle_seed)
    random.shuffle(zipped)
    images, labels = zip(*zipped)
    return images, labels

class DeepSolarisDatasetFilesNested(_DeepSolarisDataset):
    """A datset of images from a directory with nested subdirectories that
    contain images of one class each.
    """

    def __init__(self, name, root_data_dir, transform=None, normalize=True,
                 device='cpu', image_extension='.tiff', classes=None,
                 shuffle_seed=42, max_size=None):
        super().__init__(name, root_data_dir, transform, ToTensor(), normalize, device, max_size)
        self._images, self._labels = _find_images_labels_from_nested_dir(
            root_data_dir, name, image_extension, classes, shuffle_seed)

    def _size(self):
        return len(self._images)

    def _get_image_label(self, idx):
        image = open_image(self._images[idx]).convert('RGB')
        label = self._labels[idx]
        return image, label

def _agg_sampler_labels(sampler, dataset):
    return [dataset._labels[i] for i in sampler]

def loader_summary(loader):
    """Returns a summary string with information about the data in the loader."""
    labels = _agg_sampler_labels(loader.sampler, loader.dataset)
    dataset_size = len(labels)
    positives = sum(labels)
    negatives = dataset_size - positives
    str_ = """Dataset dir and name:  {}/{}
Dataset size:          {}
Positives:             {}
Negatives:             {}
Positives/total:       {:.3f}
Imbalance coefficient: {:.3f}
Transforms:            {}
Normalization:         {}
""".format(
        loader.dataset._data_dir, loader.dataset._name,
        dataset_size, positives, negatives,
        positives/dataset_size,
        2 * positives / dataset_size - 1,
        loader.dataset._transform,
        loader.dataset._norm)
    return str_

def loaders_summary(train_loader, validation_loader, test_loader, extra_test_loader):
    str_ = """# Train data
{}

# Validation data
{}

# Test data
{}

# Extra data
{}
""".format(loader_summary(train_loader),
           loader_summary(validation_loader),
           loader_summary(test_loader),
           loader_summary(extra_test_loader))
    return str_

def loaders_summary_generic(names, loaders):
    str_ = ""
    for name in names:
        str_ += "# {}\n".format(name) + "{}\n\n"
    summaries = [
        loader_summary(loader) for loader in loaders
    ]
    return str_.format(*summaries)
