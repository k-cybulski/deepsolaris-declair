"""
Special transforms which apply to both the image and its mask, taking into
account that the mask is binary and ought to be transformed in the same way as
the image.

Copied almost fully from the PyTorch vision repository:
https://github.com/pytorch/vision/blob/master/references/segmentation/transforms.py
"""
import numpy as np
from PIL import Image
import random
import numbers

import torch
from torchvision import transforms as T
from torchvision.transforms import functional as F


def pad_if_smaller(img, size, fill=0):
    min_size = min(img.size)
    if min_size < size:
        ow, oh = img.size
        padh = size - oh if oh < size else 0
        padw = size - ow if ow < size else 0
        img = F.pad(img, (0, 0, padw, padh), fill=fill)
    return img


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, target):
        for t in self.transforms:
            image, target = t(image, target)
        return image, target


class RandomResize(object):
    def __init__(self, min_size, max_size=None):
        self.min_size = min_size
        if max_size is None:
            max_size = min_size
        self.max_size = max_size

    def __call__(self, image, target):
        size = random.randint(self.min_size, self.max_size)
        image = F.resize(image, size)
        target = F.resize(target, size, interpolation=Image.NEAREST)
        return image, target

class RandomRotation(object):
    def __init__(self, degrees):
        if isinstance(degrees, numbers.Number):
            if degrees < 0:
                raise ValueError("Degrees must be either a tuple or positive.")
            self._degrees = [-float(degrees), float(degrees)]
        else:
            self._degrees = [float(k) for k in degrees]

    def __call__(self, image, target):
        angle = float(torch.empty(1).uniform_(self._degrees[0], self._degrees[1]).item())
        image = F.rotate(image, angle)
        target = F.rotate(target, angle, resample=Image.NEAREST)
        return image, target


class RandomHorizontalFlip(object):
    def __init__(self, flip_prob):
        self.flip_prob = flip_prob

    def __call__(self, image, target):
        if random.random() < self.flip_prob:
            image = F.hflip(image)
            target = F.hflip(target)
        return image, target


class RandomCrop(object):
    def __init__(self, size):
        self.size = size

    def __call__(self, image, target):
        image = pad_if_smaller(image, self.size)
        target = pad_if_smaller(target, self.size, fill=255)
        crop_params = T.RandomCrop.get_params(image, (self.size, self.size))
        image = F.crop(image, *crop_params)
        target = F.crop(target, *crop_params)
        return image, target


class CenterCrop(object):
    def __init__(self, size):
        self.size = size

    def __call__(self, image, target):
        image = F.center_crop(image, self.size)
        target = F.center_crop(target, self.size)
        return image, target


class ToTensor(object):
    def __call__(self, image, target):
        image = F.to_tensor(image)
        target = torch.as_tensor(np.array(target).transpose((2, 0, 1)), dtype=torch.int64)
        return image, target


class Normalize(object):
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, image, target):
        image = F.normalize(image, mean=self.mean, std=self.std)
        return image, target


def to_tensor(image, mask):
    """Transforms a PIL image and mask pair into tensors, taking integrality of
    the mask into account.
    """
    image = F.to_tensor(image)
    mask = torch.as_tensor(np.array(mask, ndmin=3).transpose((2, 0, 1))/255, dtype=torch.int64)
    return image, mask

def mask_to_one_channel(mask):
    # the three channels of the mask should be the same, so we can take any of
    # them
    return mask[0]

def _to_numpy(image_tensor):
    # helper used to plot tensors in matplotlib
    return np.transpose(image_tensor.numpy(), (1, 2, 0))
