from os.path import join
from os import listdir
import random

import numpy as np
from torch.utils.data import Dataset
from torchvision.io import read_image
from torchvision.transforms import functional as F
import torch
from PIL.Image import open as open_image

from .pair_transforms import to_tensor, mask_to_one_channel
from ..transforms import standard_norm

def find_file_pairs(directory, extension='.tiff'):
    """Returns a list of tuples of colour image and mask file paths.

    Files are expected to be named as such:
     - <root><extension> - color image
     - <root>_mask<extension> - mask
    """
    # returns a sorted, filtered list of file roots of files in directory
    wanted_files_in_dir = sorted(map(lambda x: x[:x.rfind(extension)],
                                     filter(lambda x: x.endswith(extension),
                                            listdir(directory))))
    file_pairs = []
    for i in range(0, len(wanted_files_in_dir) - 1):
        if (wanted_files_in_dir[i+1].endswith('_mask') and
                wanted_files_in_dir[i+1].startswith(wanted_files_in_dir[i])):
            pair = (join(directory, wanted_files_in_dir[i] + extension),
                    join(directory, wanted_files_in_dir[i+1] + extension))
            file_pairs.append(pair)
    return file_pairs

def find_file_filtered(directory, extension='.tiff',
                       filter_lambda=lambda x: "_mask" not in x):
    """Returns a list of file paths of a given extension which are accepted by
    a given lambda function.

    This is used to filter out `_mask` files if we are only interested in base
    images.
    """
    # returns a sorted, filtered list of file roots of files in directory
    wanted_files_in_dir = sorted(
        filter(lambda x: x.endswith('.tiff') and filter_lambda(x), listdir(directory)))
    return [join(directory, file_) for file_ in wanted_files_in_dir]


class SegmentationDataset(Dataset):
    def __init__(self, name, root_data_dir, pair_transform=None, normalize=True,
                 device='cpu', extension='.tiff', max_size=None, shuffle_seed=42):
        self._device = device
        self._pair_transform = pair_transform
        self._norm = (standard_norm() if normalize else None)
        self._name = name
        self._root_data_dir = root_data_dir
        self._data_dir = join(root_data_dir, name)
        self._extension = extension
        # Sort to ensure reproducibility between systems
        file_pairs = sorted(
            find_file_pairs(self._data_dir, extension=extension),
            key=lambda pair: pair[0]
        )
        # Shuffle to make sure that subsets of the dataset are equally random
        random.seed(shuffle_seed)
        random.shuffle(file_pairs)
        self._file_pairs = file_pairs
        self._size = len(self._file_pairs)
        if max_size is not None:
            self._size = min(max_size, self._size)
        # torchvision 0.8.0 only supports these files
        self._use_torchvision_io = (extension in ['.jpg', '.jpeg', '.png'])
        if self._use_torchvision_io:
            raise NotImplementedError("Correct handling of .jpg, .jpeg and .png not implemented yet. See __getitem__ code")

    def __len__(self):
        return self._size

    def __getitem__(self, idx):
        path_image, path_mask = self._file_pairs[idx]
        if self._use_torchvision_io:
            image = read_image(path_image)
            mask = read_image(path_mask)
            # TODO: Convert mask to grayscale
            raise NotImplementedError("Correct handling of .jpg, .jpeg and .png not implemented yet.")
        else:
            # conversion is here to remove the alpha channel
            image = open_image(path_image).convert('RGB')
            mask = open_image(path_mask).convert('RGB')
            image, mask = to_tensor(image, mask)
        if self._norm:
            image = self._norm(image)
        if self._pair_transform:
            image, mask = self._pair_transform(image, mask)
        mask = mask_to_one_channel(mask)
        image = image.to(self._device)
        mask = mask.to(self._device)
        return image, mask

class ReconstructionDataset(Dataset):
    def __init__(self, name, root_data_dir, transform=None, noise=None, normalize=True,
                 device='cpu', extension='.tiff', max_size=None, shuffle_seed=42):
        self._device = device
        self._transform = transform
        self._noise = noise
        self._norm = (standard_norm() if normalize else None)
        self._name = name
        self._root_data_dir = root_data_dir
        self._data_dir = join(root_data_dir, name)
        self._extension = extension
        # Sort to ensure reproducibility between systems
        files = sorted(find_file_filtered(self._data_dir, extension=extension))
        # Shuffle to make sure that subsets of the dataset are equally random
        random.seed(shuffle_seed)
        random.shuffle(files)
        self._files = files
        self._size = len(self._files)
        if max_size is not None:
            self._size = min(max_size, self._size)
        # torchvision 0.8.0 only supports these files
        self._use_torchvision_io = (extension in ['.jpg', '.jpeg', '.png'])
        if self._use_torchvision_io:
            raise NotImplementedError("Correct handling of .jpg, .jpeg and .png not implemented yet. See __getitem__ code")

    def __len__(self):
        return self._size

    def __getitem__(self, idx):
        path_image = self._files[idx]
        if self._use_torchvision_io:
            image = read_image(path_image)
            raise NotImplementedError("Correct handling of .jpg, .jpeg and .png not implemented yet.")
        else:
            # conversion is here to remove the alpha channel
            image = open_image(path_image).convert('RGB')
            image = F.to_tensor(image)
        if self._norm:
            image = self._norm(image)
        if self._transform:
            image = self._transform(image)
        if self._noise:
            input_image = self._noise(image.clone())
        else:
            input_image = image
        input_image = input_image.to(self._device)
        image = image.to(self._device)
        return input_image, image

class LazyDataTupleOutOfBoundsException(Exception):
    """Raised when more data is attempted to be taken from a LazyDataTuple than
    possible.
    """

# In order to save memory during training from multiple datasets, the lazy data
# tuple below allows for taking one batch at a time from each of the loaders
# used, rather than taking all of them in at once.
class LazyDataTuple:
    def __init__(self, iterators, data_tuple_loader):
        # Ensure that if the main (first) iterator ends, the iteration of the
        # for loop is over and the data tuple loader creates a new iterator in
        # place of the first one. The other iterators will stay at whatever
        # point they were.
        try:
            self._first = next(iterators[0])
        except StopIteration:
            data_tuple_loader._create_new_iterator(0)
            raise StopIteration
        self._iterators = iterators
        self._data_tuple_loader = data_tuple_loader
        self._idx = 0

    def take(self):
        if self._idx >= len(self._iterators):
            raise LazyDataTupleOutOfBoundsException
        if self._idx == 0:
            out = self._first
        else:
            try:
                out = next(self._iterators[self._idx])
            except StopIteration:
                self._data_tuple_loader._create_new_iterator(self._idx)
                out = next(self._iterators[self._idx])
        self._idx += 1
        return out

class DataTupleLoader:
    def __init__(self, *loaders):
        self._loaders = loaders
        self._iters = [iter(loader) for loader in loaders]

    def __len__(self):
        return len(self._loaders[0])

    def __next__(self):
        return LazyDataTuple(self._iters, self)

    def __iter__(self):
        return self

    def _create_new_iterator(self, idx):
        self._iters[idx] = iter(self._loaders[idx])

