"""
This file contains execution functions for auxiliary task pretraining
experiments. The experiments are:
 - A reconstrution task, where the model has to recreate a (noisy) image from its hidden representation, i.e. it's an autoencoder.
 - A segmentation task, where the model has to segment images into classes.

Best models are saved.
"""

from datetime import datetime

import torch
from torch.cuda import memory_stats
from matplotlib import pyplot as plt
from ignite.engine import Engine, Events
from ignite.metrics import Loss
from ignite.handlers import EarlyStopping, TerminateOnNan
from torchsummary import summary
from tempfile import NamedTemporaryFile
import json

from declair import call
from declair.search.search_info import is_metric_within_best_so_far
from declair.results import add_artifact_string, add_artifact_fig
from declair.frameworks.ignite.evaluators import (MetricsAttacher,
                                                  SacredMetricsLogger)
from declair.frameworks.ignite.gpu import clear_device_cache

from ...evaluation.show_images import fig_triplets
from ...evaluation.segmentation_metrics import IntersectionOverUnion, segmentation_decision
from ...evaluation.simple_metrics import OutputLoss

def _output_loss_transform(output):
    x, y, y_pred, loss = output
    return (x, loss)

def _output_targets_transform(output):
    x, y, y_pred, loss = output
    return (y, y_pred)

def _get_cuda_memory_stats_str(device):
    str_ = json.dumps(memory_stats(device=device), indent=4)
    return str_

def _execute_pretrain(params, search_info, _run, extra_metrics, plot_engine_images_func, optimized_metric, optimized_metric_top):
    torch.set_num_threads(params['num_threads'])

    model = call(params['architecture']['model']).to(params['device'])
    criterion = call(params['architecture']['criterion'])
    optimizer = call(params['optimizer'], model.parameters())
    
    train_loader, validation_loader, test_loader = call(params['dataset'])
    
    max_epochs = params['max_epochs']

    def update_model(engine, batch):
        model.train()
        optimizer.zero_grad()
        inputs, targets = batch
        y_pred = model(inputs, predict=False, decode=True)
        loss = criterion(y_pred, targets)
        loss.backward()
        optimizer.step()
        return inputs, targets, y_pred, loss

    def just_compute_loss(engine, batch):
        model.eval()
        inputs, targets = batch
        with torch.no_grad():
            y_pred = model(inputs, predict=False, decode=True)
            loss = criterion(y_pred, targets)
            return inputs, targets, y_pred, loss

    train_engine = Engine(update_model)
    val_engine = Engine(just_compute_loss)
    test_engine = Engine(just_compute_loss)

    @train_engine.on(Events.EPOCH_STARTED(every=1))
    @val_engine.on(Events.EPOCH_STARTED(every=1))
    @test_engine.on(Events.EPOCH_STARTED(every=1))
    def clear_cache(engine):
        clear_device_cache(params['device'])
    
    # Run the model on the validation set after every training epoch.
    @train_engine.on(Events.EPOCH_COMPLETED(every=1))
    def run_val_engine(engine):
        val_engine.run(validation_loader)

    # Run the model on the test set at the very end of training.
    @train_engine.on(Events.COMPLETED)
    def run_test_engine(engine):
        test_engine.run(test_loader)

    @train_engine.on(Events.ITERATION_COMPLETED(every=50))
    def training_progress_snippet(engine):
        timestamp = datetime.utcnow().isoformat()
        print("Epoch {}: {} at batch {}/{} with loss {:.3f}".format(
            engine.state.epoch,
            timestamp,
            engine.state.iteration % len(engine.state.dataloader),
            len(engine.state.dataloader),
            engine.state.output[3]
        ))

    def plot_engine_images(engine, name, short_name):
        plot_engine_images_func(engine, name, short_name, _run, train_engine)

    @val_engine.on(Events.ITERATION_COMPLETED(once=1))
    def plot_val_images(engine):
        plot_engine_images(engine, "Validation", "val")

    @test_engine.on(Events.ITERATION_COMPLETED(once=1))
    def plot_test_images(engine):
        plot_engine_images(engine, "Test", "test")

    metrics_calls = {
        'loss': {"call": OutputLoss,
                 "params": {"output_transform": _output_loss_transform}},
        **extra_metrics
    }

    evaluators = [
        MetricsAttacher(metrics=metrics_calls),
        SacredMetricsLogger()
    ]

    for evaluator in evaluators:
        evaluator.attach(_run, ['train', 'val', 'test'],
                         train_engine, val_engine, test_engine)

    # Early termination
    train_engine.add_event_handler(Events.ITERATION_COMPLETED, TerminateOnNan())
    def early_stopping_score_function(engine):
        val_loss = engine.state.metrics['loss']
        return -val_loss
    early_stopping_handler = EarlyStopping(patience=2,
                                    score_function=early_stopping_score_function,
                                    trainer=train_engine)
    val_engine.add_event_handler(Events.COMPLETED, early_stopping_handler)


    add_artifact_string(_run,
                        str(summary(model, (3, 400, 400), device=params['device'], verbose=0)),
                        'model_summary')

    # Some command line printouts before start of training
    # they're not so important to store in Sacred
    start_str = """
Training batches: {}
Validation baches: {}
Testing batches: {}
""".format(
    len(train_loader),
    len(validation_loader),
    len(test_loader))
    print(start_str)

    train_engine.run(train_loader, max_epochs=max_epochs)

    # val_engine.state may be none if TerminateOnNan executes during first
    # batch and stops training too early for the validation engine to ever run.
    if val_engine.state is not None:
        if 'best_so_far' in search_info:
            to_remember = search_info['best_so_far']['__to_remember__']
            if is_metric_within_best_so_far('val_{}'.format(optimized_metric), val_engine.state.metrics[optimized_metric], search_info['best_so_far'], to_remember, optimized_metric_top):
                # saving the model
                with NamedTemporaryFile(mode='wb', suffix='.pkl') as tmp_file:
                    torch.save(model, tmp_file)
                    _run.add_artifact(tmp_file.name, 'model.pkl')
        return {
            **{'train_{}'.format(key): value for key, value in train_engine.state.metrics.items()},
            **{'val_{}'.format(key): value for key, value in val_engine.state.metrics.items()},
            **{'test_{}'.format(key): value for key, value in test_engine.state.metrics.items()},
        }
    else:
        return {
            **{'train_{}'.format(key): value for key, value in train_engine.state.metrics.items()},
            **{'test_{}'.format(key): value for key, value in test_engine.state.metrics.items()},
        }

def _plot_engine_images_reconstruction(engine, name, short_name, _run, train_engine):
    x, y, y_pred, loss = engine.state.output
    epoch = train_engine.state.epoch
    title = "{} set sample on epoch {}".format(name, epoch)
    fig = fig_triplets(x, y, y_pred, plt, title, denormalize=True)
    add_artifact_fig(_run, fig, "{}_sample_{}.png".format(short_name, epoch))
    plt.close()

def _plot_engine_images_segmentation(engine, name, short_name, _run, train_engine):
    x, y, y_pred, loss = engine.state.output
    y_pred = segmentation_decision(y_pred)
    epoch = train_engine.state.epoch
    title = "{} set sample on epoch {}".format(name, epoch)
    fig = fig_triplets(x, y, y_pred, plt, title, denormalize=[0])
    add_artifact_fig(_run, fig, "{}_sample_{}.png".format(short_name, epoch))
    plt.close()

def execute_pretrain_reconstruction(params, search_info, _run):
    """Trains a reconstruction model, i.e. autoencoder."""
    return _execute_pretrain(params, search_info, _run, {}, _plot_engine_images_reconstruction, 'loss', False)

def execute_pretrain_segmentation(params, search_info, _run):
    """Trains a segmentation model."""
    extra_metrics_calls = {
        'iou': {"call": IntersectionOverUnion,
                 "params": {"output_transform": _output_targets_transform}}
    }
    return _execute_pretrain(params, search_info, _run, extra_metrics_calls, _plot_engine_images_segmentation, 'iou', True)
