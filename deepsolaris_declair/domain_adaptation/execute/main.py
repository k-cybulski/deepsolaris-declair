"""
This file contains execution functions for the target task experiments, i.e.
solar panel prediction with domain adaptation.

Structure of an experiment:
1. A model is constructed in one of the following ways:
 - Initialized from scratch.
 - Loaded from a prior pretraining experiment.
 - Loaded from the PyTorch database (i.e. pretrained on ImageNet)
2. Stage 1 of training: It is trained on the target and auxiliary tasks at once
   on data from the source domain.
 - After every epoch of training, it is evaluated on:
   - target task on source domain
   - auxiliary task on source domain
 - The combined weighted loss guides early stopping
 - At the end of training, it is evaluated on all tasks on the validation sets.
3. Stage 2 of training: It is trained on the target task on the source domain
   and auxiliary task on the target domain.
 - After every epoch of training, it is evaluated on:
   - target task on source domain
   - auxiliary task on target domain
 - The combined weighted loss guides early stopping
 - At the end of training, it is evaluated on all tasks on the validation and
   test sets.
"""
from copy import copy
from datetime import datetime
from tempfile import NamedTemporaryFile

import torch
from ignite.engine import Engine, Events
from torchsummary import summary

from declair import call
from declair.results import add_artifact_string
from declair.search.search_info import is_metric_within_best_so_far
from declair.frameworks.ignite.gpu import clear_device_cache
from declair.frameworks.ignite.evaluators import SacredMetricsLogger
from ignite.metrics import Accuracy, Precision, Recall
from ignite.handlers import EarlyStopping, TerminateOnNan

from .pretraining import (_plot_engine_images_reconstruction,
                          _plot_engine_images_segmentation,
                          _output_targets_transform)
from ..data import DataTupleLoader
from ...data import loaders_summary_generic
from ...evaluation.class_balance import ClassBalanceAccuracy
from ...evaluation.segmentation_metrics import IntersectionOverUnion
from ...evaluation.simple_metrics import OutputLoss

def _execute(params, search_info, _run,
             plot_engine_images_unwrapped, extra_aux_metrics,
             optimized_metric, optimized_metric_top):
    model = call(params['architecture']['model']).to(params['device'])
    target_criterion = call(params['architecture']['criterion_target'])
    aux_criterion = call(params['architecture']['criterion_aux'])
    optimizer = call(params['optimizer'], model.parameters())

    use_last_layer_to_compute_aux_loss = params[
        'use_last_layer_to_compute_aux_loss']

    # Prepare data loading
    ## Source domain target task loaders
    (source_target_train_loader, source_target_val_loader,
     source_target_test_loader) = call(params['dataset']['source_target'])
    ## Source domain auxiliary task loaders
    (source_aux_train_loader, source_aux_val_loader,
     source_aux_test_loader) = call(params['dataset']['source_aux'])
    ## Target domain target task loaders (for validation)
    (_, target_target_val_loader,
     target_target_test_loader) = call(params['dataset']['target_target'])
    ## Target domain auxiliary task loaders
    (target_aux_train_loader, target_aux_val_loader,
     target_aux_test_loader) = call(params['dataset']['target_aux'])
    ## Tuple loaders for loading two datasets at once smartly
    train_tuple_loader_stage_1 = DataTupleLoader(
        source_target_train_loader,
        source_aux_train_loader
    )
    train_tuple_loader_stage_2 = DataTupleLoader(
        source_target_train_loader,
        target_aux_train_loader
    )

    ## Miscellaneous
    max_epochs_stage_1 = params['max_epochs']['stage_1']
    max_epochs_stage_2 = params['max_epochs']['stage_2']

    loss_weight_lambda = params['loss_weight_lambda']

    # Define training and evaluation functions
    def update_model(engine, data_tuple):
        model.train()
        optimizer.zero_grad()
        # Target task
        inputs, targets = data_tuple.take()
        # keep this so we can return it; don't care about inputs, though
        target_targets = targets
        target_y_pred = model(inputs, predict=True, decode=False,
                              use_last_layer=False)
        target_loss = target_criterion(target_y_pred, targets)
        target_y_pred = model.last_layer(predict=target_y_pred)
        # Auxiliary task
        inputs, targets = data_tuple.take()
        if use_last_layer_to_compute_aux_loss:
            y_pred = model(inputs, predict=False, decode=True, 
                           use_last_layer=True)
            aux_loss = aux_criterion(y_pred, targets)
        else:
            y_pred = model(inputs, predict=False, decode=True, 
                           use_last_layer=False)
            aux_loss = aux_criterion(y_pred, targets)
            y_pred = model.last_layer(decode=y_pred)
        # Combined
        loss = (loss_weight_lambda * target_loss 
                + (1 - loss_weight_lambda) * aux_loss)
        loss.backward()
        optimizer.step()
        return target_targets, target_y_pred, inputs, targets, y_pred, loss

    def _just_compute_loss(engine, batch, criterion, target_task, use_last_layer, predict_or_decode):
        model.eval()
        inputs, targets = batch
        with torch.no_grad():
            if use_last_layer:
                y_pred = model(inputs, 
                               predict=target_task, decode=(not target_task), 
                               use_last_layer=True)
                loss = criterion(y_pred, targets)
            else:
                y_pred = model(inputs, 
                               predict=target_task, decode=(not target_task), 
                               use_last_layer=False)
                loss = criterion(y_pred, targets)
                kwargs = {
                    predict_or_decode: y_pred
                }
                y_pred = model.last_layer(**kwargs)
            return inputs, targets, y_pred, loss

    def just_compute_target_loss(engine, batch):
        return _just_compute_loss(engine, batch, target_criterion, True, False, 'predict')

    def just_compute_aux_loss(engine, batch):
        return _just_compute_loss(engine, batch, aux_criterion, False, use_last_layer_to_compute_aux_loss, 'decode')

    stage = 1

    train_engine = Engine(update_model)

    val_engines = {
        'source': {
            'target': Engine(just_compute_target_loss),
            'aux': Engine(just_compute_aux_loss)
        },
        'target': {
            'target': Engine(just_compute_target_loss),
            'aux': Engine(just_compute_aux_loss)
        }
    }

    test_engines = {
        'source': {
            'target': Engine(just_compute_target_loss),
            'aux': Engine(just_compute_aux_loss)
        },
        'target': {
            'target': Engine(just_compute_target_loss),
            'aux': Engine(just_compute_aux_loss)
        }
    }

    # Metrics
    ## Classification task metrics
    def _classification_metric_target_output_transform(output):
        x, y, y_pred, loss = output
        return (y_pred > 0.5)[:, 1] * 1., y

    def _trainer_classification_metric_target_output_transform(output):
        (target_targets, target_y_pred, _, _, _, loss) = output
        return _classification_metric_target_output_transform(
            (target_targets, target_targets, target_y_pred, loss))

    target_task_classification_metric_types = {
        'accuracy': Accuracy,
        'precision': Precision,
        'recall': Recall,
        'cb_accuracy': ClassBalanceAccuracy
    }
    target_task_other_metric_types = {
        'loss': OutputLoss
    }
    ### Train engine has to be handled separately from other engines because of
    ### its output being different
    target_task_engines = [
        val_engines['source']['target'],
        val_engines['target']['target'],
        test_engines['source']['target'],
        test_engines['target']['target']
    ]
    for metric_name, metric_type in target_task_classification_metric_types.items():
        metric_obj = metric_type(
            output_transform=_trainer_classification_metric_target_output_transform)
        metric_obj.attach(train_engine, metric_name)
        for engine in target_task_engines:
            metric_obj = metric_type(
                output_transform=_classification_metric_target_output_transform)
            metric_obj.attach(engine, metric_name)
    ### These engines don't require transforms
    for engine in target_task_engines + [train_engine]:
        for metric_name, metric_type in target_task_other_metric_types.items():
            metric_obj = metric_type()
            metric_obj.attach(engine, metric_name)

    ## Auxiliary task metrics
    aux_metric_calls = {
        'loss': {"call": OutputLoss},
        **extra_aux_metrics
    }
    def _trainer_metric_aux_output_transform(output):
        (target_targets, target_y_pred, inputs, targets, y_pred, loss) = output
        return inputs, targets, y_pred, loss
    auxiliary_task_engines = [
        val_engines['source']['aux'],
        val_engines['target']['aux'],
        test_engines['source']['aux'],
        test_engines['target']['aux']
    ]
    for metric_name, metric_call_dict in aux_metric_calls.items():
        if 'output_transform' not in metric_call_dict.get('params', {}):
            trainer_output_transform = _trainer_metric_aux_output_transform
        else:
            trainer_output_transform = lambda x: (
                metric_call_dict.get('params', {}).get('output_transform')(
                    _trainer_metric_aux_output_transform(x)
                )
            )
        trainer_metric_call_dict = copy(metric_call_dict)
        trainer_metric_params = copy(trainer_metric_call_dict.get('params', {}))
        trainer_metric_params['output_transform'] = trainer_output_transform
        trainer_metric_call_dict['params'] = trainer_metric_params
        metric_obj = call(trainer_metric_call_dict)
        metric_obj.attach(train_engine, metric_name)
        for engine in auxiliary_task_engines:
            metric_obj = call(metric_call_dict)
            metric_obj.attach(engine, metric_name)

    # Cache clearing before every epoch
    for engine in ([train_engine] 
                   + target_task_engines 
                   + auxiliary_task_engines):
        engine.add_event_handler(
            Events.EPOCH_STARTED(every=1), 
            lambda engine: clear_device_cache(params['device']))

    # Helper info printouts
    @train_engine.on(Events.ITERATION_COMPLETED(every=50))
    def training_progress_snippet(engine):
        timestamp = datetime.utcnow().isoformat()
        print("Epoch {}: {} at batch {}/{} with loss {:.3f}".format(
            engine.state.epoch,
            timestamp,
            engine.state.iteration % len(engine.state.dataloader),
            len(engine.state.dataloader),
            engine.state.output[-1]
        ))

    # Plotting auxiliary task images
    def plot_engine_images(engine, name, short_name):
        # The long name is for being legible as a title of a plot
        name_with_stage = "{} stage {}".format(name, stage)
        # The short name is for filenames, so let's put stage at the beginning
        # for nicer file browser sorting
        short_name_with_stage = "stage_{}_{}".format(stage, short_name)
        plot_engine_images_unwrapped(engine, name_with_stage, short_name_with_stage, _run, train_engine)

    for domain in ['source', 'target']:
        str_ = "Validation {}".format(domain)
        str_short = "val_{}".format(domain)
        val_engines[domain]['aux'].add_event_handler(
            Events.ITERATION_COMPLETED(once=1),
            plot_engine_images,
            str_, str_short)
        str_ = "Test {}".format(domain)
        str_short = "test_{}".format(domain)
        test_engines[domain]['aux'].add_event_handler(
            Events.ITERATION_COMPLETED(once=1),
            plot_engine_images,
            str_, str_short)

    # Early termination
    train_engine.add_event_handler(Events.ITERATION_COMPLETED, TerminateOnNan())
    ## Training will be terminated if the validation loss on the combined tasks
    ## stops improving.
    ## We keep track of which engines are responsible for the combined loss
    ## that we care about in these variables, since they will change between
    ## the stages and we want the early stopping function to know that
    early_stopping_target_engine = None
    early_stopping_aux_engine = None
    ## Note that we will need to order the aux and target validation engines to
    ## run in such an order that the early stopping check handle will be executed
    ## only after the second engine is finished. As such, we will first run the
    ## target engine, then the aux engine and attach the early stopping handler to
    ## the aux engine.
    def early_stopping_score_function(engine):
        target_loss = early_stopping_target_engine.state.metrics['loss']
        aux_loss = early_stopping_aux_engine.state.metrics['loss']
        val_loss = (loss_weight_lambda * target_loss
                + (1 - loss_weight_lambda) * aux_loss)
        metric_name = 'stage_{}_val_combined_loss'.format(stage)
        _run.log_scalar(metric_name, val_loss, step=train_engine.state.epoch)
        return -val_loss

    # Add some run information
    add_artifact_string(_run,
                        str(summary(model, (3, 400, 400), device=params['device'], verbose=0)),
                        'model_summary')

    add_artifact_string(_run,
                        loaders_summary_generic(["Source train", 
                                                 "Source validation", 
                                                 "Source test",
                                                 "Target validation", 
                                                 "Target test"],
                                                [source_target_train_loader,
                                                 source_target_val_loader,
                                                 source_target_test_loader,
                                                 target_target_val_loader,
                                                 target_target_test_loader]),
                        'data_summary_classification')

    # Stage 1
    early_stopping_target_engine = val_engines['source']['target']
    early_stopping_aux_engine = val_engines['source']['aux']
    stage_1_early_stopping_handler = EarlyStopping(patience=2,
                                    score_function=early_stopping_score_function,
                                    trainer=train_engine)
    stage_1_early_stopping_removable_handle = val_engines['source']['aux'].add_event_handler(
        Events.COMPLETED, stage_1_early_stopping_handler)

    ## After every training epoch, run the model on the source target and auxiliary datsets
    ## At the end of training, run it on the target domain on both tasks as well
    stage_1_removable_handlers = [
        train_engine.add_event_handler(
            Events.EPOCH_COMPLETED(every=1),
            lambda engine: val_engines['source']['target'].run(source_target_val_loader)
        ),
        # ensure that the source+aux engine runs after the source+target, since
        # that's what the early stopping score function needs
        val_engines['source']['target'].add_event_handler(
            Events.EPOCH_COMPLETED(every=1),
            lambda engine: val_engines['source']['aux'].run(source_aux_val_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: val_engines['target']['target'].run(target_target_val_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: val_engines['target']['aux'].run(target_aux_val_loader)
        ),
    ]
    sacred_metrics_logger = SacredMetricsLogger()
    stage_1_engine_names = [
        'stage_1_train', 
        'stage_1_val_source_target', 'stage_1_val_source_aux',
        'stage_1_val_target_target', 'stage_1_val_target_aux']
    stage_1_engines = [
        train_engine, 
        val_engines['source']['target'], val_engines['source']['aux'],
        val_engines['target']['target'], val_engines['target']['aux']
    ]
    sacred_metrics_logger.attach(
        _run,
        stage_1_engine_names,
        *stage_1_engines)

    train_engine.run(train_tuple_loader_stage_1, max_epochs=max_epochs_stage_1)
    
    stage_1_metrics = {}
    for name, engine in zip(stage_1_engine_names, stage_1_engines):
        if engine.state is not None:
            stage_1_metrics.update(
                {'{}_{}'.format(name, key): value 
                 for key, value in engine.state.metrics.items()}
            )

    ## If the model performs badly, stop here. "Badly" will in this case mean
    ## cb_accuracy below 0.8 on the source domain, since from experience it's a
    ## rather easily obtainable score which means that the model knows
    ## *something*
    stage_1_important_metric = 'stage_1_val_source_target_cb_accuracy'
    if (stage_1_important_metric not in stage_1_metrics
            or stage_1_metrics[stage_1_important_metric] < 0.8):
        print("Not proceeding to stage 2: {} below 0.8".format(
            stage_1_important_metric))
        return stage_1_metrics

    # If we have a very good model at this point, save it. We might use it
    # later for experiments on improving domain adaptation.
    if 'best_so_far' in search_info:
        to_remember = search_info['best_so_far']['__to_remember__']
        if is_metric_within_best_so_far(stage_1_important_metric,
                                        stage_1_metrics[stage_1_important_metric],
                                        search_info['best_so_far'],
                                        to_remember, True):
            # saving the model
            with NamedTemporaryFile(mode='wb', suffix='.pkl') as tmp_file:
                torch.save(model, tmp_file)
                _run.add_artifact(tmp_file.name, 'model_stage_1.pkl')

    # Stage 2
    stage = 2
    ## Remove callbacks which define what to execute when
    stage_1_early_stopping_removable_handle.remove()
    for handler in stage_1_removable_handlers:
        handler.remove()
    ## Detach the metrics logger, because all metrics it logs are marked with
    ## `stage_1`
    sacred_metrics_logger.detach()

    ## Go forth with defining the second stage
    early_stopping_target_engine = val_engines['source']['target']
    early_stopping_aux_engine = val_engines['target']['aux']

    stage_2_early_stopping_handler = EarlyStopping(patience=2,
                                    score_function=early_stopping_score_function,
                                    trainer=train_engine)
    stage_2_early_stopping_removable_handle = val_engines['target']['aux'].add_event_handler(
        Events.COMPLETED, stage_2_early_stopping_handler)

    ## After every training epoch, run the model on the source target and on
    ## the target auxiliary datasets
    ## At the end of training, run it on the remaining two task/datasets for validation
    ## Also, run the model on the test sets
    stage_2_removable_handlers = [
        train_engine.add_event_handler(
            Events.EPOCH_COMPLETED(every=1),
            lambda engine: val_engines['source']['target'].run(source_target_val_loader)
        ),
        # ensure that the source+aux engine runs after the source+target, since
        # that's what the early stopping score function needs
        val_engines['source']['target'].add_event_handler(
            Events.EPOCH_COMPLETED(every=1),
            lambda engine: val_engines['target']['aux'].run(target_aux_val_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: val_engines['target']['target'].run(target_target_val_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: val_engines['source']['aux'].run(source_aux_val_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: test_engines['target']['target'].run(target_target_test_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: test_engines['source']['aux'].run(source_aux_test_loader)
        ),
        train_engine.add_event_handler(
            Events.COMPLETED,
            lambda engine: test_engines['target']['aux'].run(target_aux_test_loader)
        ),
    ]
    stage_2_engine_names = [
        'stage_2_train', 
        'stage_2_val_source_target', 'stage_2_val_source_aux',
        'stage_2_val_target_target', 'stage_2_val_target_aux',
        'stage_2_test_source_target', 'stage_2_test_source_aux',
        'stage_2_test_target_target', 'stage_2_test_target_aux']
    stage_2_engines = [
        train_engine, 
        val_engines['source']['target'], val_engines['source']['aux'],
        val_engines['target']['target'], val_engines['target']['aux'],
        test_engines['source']['target'], test_engines['source']['aux'],
        test_engines['target']['target'], test_engines['target']['aux']
    ]
    sacred_metrics_logger.attach(
        _run,
        stage_2_engine_names,
        *stage_2_engines)

    # Clear out states for (possibly) saving memory
    for engine in [
            train_engine,
            # double list comprehension is a bit complicated
            *[engine for dict_ in val_engines.values() for engine in dict_.values()]
            ]:
        engine.state = None
    train_engine.run(train_tuple_loader_stage_2, max_epochs=max_epochs_stage_2)

    stage_2_metrics = {}
    for name, engine in zip(stage_2_engine_names, stage_2_engines):
        if engine.state is not None:
            stage_2_metrics.update(
                {'{}_{}'.format(name, key): value 
                 for key, value in engine.state.metrics.items()}
            )

    out_metrics = {
        **stage_1_metrics,
        **stage_2_metrics
    }

    # Save best performing models
    if 'best_so_far' in search_info:
        to_remember = search_info['best_so_far']['__to_remember__']
        if optimized_metric in out_metrics:
            if is_metric_within_best_so_far(optimized_metric,
                                            out_metrics[optimized_metric], 
                                            search_info['best_so_far'],
                                            to_remember, optimized_metric_top):
                # saving the model
                with NamedTemporaryFile(mode='wb', suffix='.pkl') as tmp_file:
                    torch.save(model, tmp_file)
                    _run.add_artifact(tmp_file.name, 'model_stage_2.pkl')

    return out_metrics

def execute_pretrain_reconstruction(params, search_info, _run):
    """Trains a reconstruction model, i.e. autoencoder."""
    return _execute(params, search_info, _run, _plot_engine_images_reconstruction, {}, 'stage_2_val_target_target_cb_accuracy', True)

def execute_segmentation(params, search_info, _run):
    extra_metrics_calls = {
        'iou': {"call": IntersectionOverUnion,
                 "params": {"output_transform": _output_targets_transform}}
    }
    return _execute(params, search_info, _run, _plot_engine_images_segmentation, extra_metrics_calls, 'stage_2_val_target_target_cb_accuracy', True)
