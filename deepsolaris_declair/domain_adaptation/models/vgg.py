import torch
import torch.nn as nn
from torchvision.models.vgg import cfgs, model_urls
from torchvision.models.utils import load_state_dict_from_url

from typing import Iterator

from declair import call

# I try to recreate how VGGs are produced in PyTorch to the extent that
# model.state_dict() is the same, so that I can use model.load_state_dict and
# fetch the pretrained VGG model parameters.

# This is how they are coded in torchvision.models.vgg
arch_to_cfg = {
    'vgg11': 'A',
    'vgg13': 'B',
    'vgg16': 'D',
    'vgg19': 'E'
}

class SequentialEncoder(nn.Module):
    """Class very similar in behaviour to nn.Sequential, but with the property
    that it keeps track of MaxPool2d indices.
    """
    def __init__(self, list_of_modules):
        super(SequentialEncoder, self).__init__()
        for idx, module in enumerate(list_of_modules):
            self.add_module(str(idx), module)

    def __iter__(self) -> Iterator[nn.Module]:
        return iter(self._modules.values())

    def __len__(self):
        return len(self._modules)

    def forward(self, x):
        indices = []
        input_sizes = []
        for module in self:
            if not isinstance(module, nn.MaxPool2d):   
                x = module(x)
            else:
                # assume all MaxPool2d have return_indices=True
                input_sizes.append(x.size())
                x, ind = module(x)
                indices.append(ind)
        return x, indices, input_sizes

class SequentialDecoder(nn.Module):
    """Class very similar in behaviour to nn.Sequential, but with the property
    that it expects to be given maxpool indices along with the input
    corresponding to each unpooling layer.
    """
    def __init__(self, list_of_modules):
        super(SequentialDecoder, self).__init__()
        for idx, module in enumerate(list_of_modules):
            self.add_module(str(idx), module)

    def __iter__(self) -> Iterator[nn.Module]:
        return iter(self._modules.values())

    def __len__(self):
        return len(self._modules)

    def forward(self, x, indices, input_sizes, use_last_layer):
        for idx, module in enumerate(self):
            if not isinstance(module, nn.MaxUnpool2d):
                if (use_last_layer 
                        or (not use_last_layer and idx != len(self) - 1)):
                    x = module(x)
            else:
                in_size = input_sizes.pop()
                ind = indices.pop()
                x = module(x, ind, in_size)
        return x

    def last_layer(self, x):
        for idx, module in enumerate(self):
            if idx == len(self) - 1:
                return module(x)

def make_encoder_layers(cfg, batch_norm):
    # taken from torchvision.models.vgg, modified to account for keeping
    # indices of maxpooling
    layers = []
    in_channels = 3
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return SequentialEncoder(layers)

def make_decoder_layers(cfg, output_type):
    """

    Args:
        cfg: a configuration from torchvision.models.vgg.cfgs
        output_type: string 'reconstruction' or integer defining number of
                     output classes per pixel.
    """
    if output_type != 'reconstruction' and not isinstance(output_type, int):
        raise ValueError("Wrong output_type for decoder")
    # makes layers which should be able to invert operation of the encoder
    layers = []
    if output_type == 'reconstruction':
        out_channels = 3
    else:
        out_channels = output_type
    rcfg = list(reversed(cfg))
    for idx, v in enumerate(rcfg):
        if v == 'M':
            layers += [nn.MaxUnpool2d(kernel_size=2, stride=2)]
        else:
            # we need to find what the next channel size is
            nidx = idx + 1
            if nidx < len(rcfg):
                while rcfg[nidx] == 'M':
                    nidx += 1
            if nidx == len(rcfg):
                # we're at the end, so return colours
                next_channels = out_channels
            else:
                next_channels = rcfg[nidx]
            conv2d = nn.ConvTranspose2d(v, next_channels, kernel_size=3, padding=1)
            layers += [conv2d]
            if nidx != len(rcfg):
                layers += [nn.ReLU(inplace=True)]
            else:
                # the final reconstruction activation is linear
                if output_type != 'reconstruction':
                    layers += [nn.Softmax(dim=1)]
    return SequentialDecoder(layers)

class VGGReconstruction(nn.Module):
    """
    Based on M. Ghifary, W. B. Kleijn, M. Zhang, D. Balduzzi, and W. Li, “Deep Reconstruction-Classification Networks for Unsupervised Domain Adaptation,” arXiv:1607.03516 [cs, stat], Aug. 2016, Accessed: Oct. 02, 2020. [Online]. Available: http://arxiv.org/abs/1607.03516.
    """
    def __init__(self, encoder, predictor, decoder, init_weights=True, init_also_encoder=True):
        super(VGGReconstruction, self).__init__()
        self.encoder = encoder
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        self.predictor = predictor
        self.decoder = decoder
        if init_weights:
            self._initialize_weights(init_also_encoder)

    def forward(self, x, predict=True, decode=True, use_last_layer=True):
        representation, indices, input_sizes = self.encoder(x)
        if predict and decode:
            pooled_representation = self.avgpool(representation)
            pooled_representation = torch.flatten(pooled_representation, 1)
            prediction = self.predictor(pooled_representation, 
                                        use_last_layer=use_last_layer)
            decoded = self.decoder(representation, indices, input_sizes, 
                                   use_last_layer=use_last_layer)
            return prediction, decoded
        elif predict:
            representation = self.avgpool(representation)
            representation = torch.flatten(representation, 1)
            return self.predictor(representation, use_last_layer=use_last_layer)
        elif decode:
            return self.decoder(representation, indices, input_sizes, 
                                use_last_layer=use_last_layer)

    def _initialize_weights(self, init_also_encoder):
        components = [self.predictor, self.decoder]
        if init_also_encoder:
            components.append(self.encoder)
        for comp in components:
            for m in comp.modules():
                if isinstance(m, (nn.Conv2d, nn.ConvTranspose2d)):
                    nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                    if m.bias is not None:
                        nn.init.constant_(m.bias, 0)
                elif isinstance(m, nn.BatchNorm2d):
                    nn.init.constant_(m.weight, 1)
                    nn.init.constant_(m.bias, 0)
                elif isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def last_layer(self, predict=None, decode=None):
        out = []
        if predict is not None:
            prediction = self.predictor.last_layer(predict)
            out.append(prediction)
        if decode is not None:
            decoded = self.decoder.last_layer(decode)
            out.append(decoded)
        if len(out) == 1:
            return out[0]
        else:
            return tuple(out)

class SequentialWithOptionalLastLayer(nn.Sequential):
    def forward(self, input, use_last_layer=True):
        for idx, module in enumerate(self):
            if (use_last_layer
                    or (not use_last_layer and idx != len(self) - 1)):
                input = module(input)
        return input

    def last_layer(self, input):
        return self[len(self) - 1](input)

def make_basic_classifier(num_classes=2):
    classifier = SequentialWithOptionalLastLayer(
        nn.Linear(512 * 7 * 7, 2500),
        nn.ReLU(True),
        nn.Dropout(),
        nn.Linear(2500, num_classes),
        nn.Softmax(dim=1),
    )
    return classifier

def vgg_reconstruction(arch, pretrained, predictor, decoder_type='reconstruction', batch_norm=False, progress=True, pytorch_model_dir=None, **kwargs):
    # predictor can be a manual call dict. See `declair.calling.call`
    if isinstance(predictor, dict) and 'call' in predictor:
        predictor = call(predictor)
    if pretrained == 'encoder':
        kwargs['init_weights'] = True
        kwargs['init_also_encoder'] = False
    else:
        kwargs['init_weights'] = True
        kwargs['init_also_encoder'] = True
    model = VGGReconstruction(
        make_encoder_layers(cfgs[arch_to_cfg[arch]], batch_norm=batch_norm), 
        predictor,
        make_decoder_layers(cfgs[arch_to_cfg[arch]], output_type=decoder_type),
        **kwargs)
    if pretrained == 'encoder':
        arch_url_key = arch
        if batch_norm:
            arch_url_key = arch_url_key + '_bn'
        state_dict = load_state_dict_from_url(model_urls[arch_url_key],
                                              progress=progress,
                                              model_dir=pytorch_model_dir)
        # the pretrained VGG models available online assume that the feature
        # section attribute is called `features`, so we need to temporarily
        # rename `encoder` into `features` to use load_state_dict
        model.features = model.encoder
        model.load_state_dict(state_dict, strict=False)
        del model.features
    return model
