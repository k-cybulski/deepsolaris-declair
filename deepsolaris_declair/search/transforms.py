from torchvision import transforms

def rotation_transform(max_degrees):
    return transforms.Compose([
        transforms.ToPILImage(),
        transforms.RandomRotation(max_degrees),
        transforms.ToTensor()
    ])

def color_jitter():
    return transforms.Compose([
        transforms.ToPILImage(),
        transforms.ColorJitter(0.5, 0.5, 0.5),
        transforms.ToTensor()
    ])
