from torch import nn
from torchvision.models import vgg16, vgg16_bn

def _num_trainable_layers(model):
    """Compute how many layers in this model's feature section are trainable."""
    num = 0
    for feature in model.features:
        params = list(feature.parameters())
        if len(params) > 0:
            num += 1
    return num

def _freeze_layers(model, layers_to_freeze):
    current_index = 0
    for feature in model.features:
        params = list(feature.parameters())
        if len(params) > 0: # This is a module which can be frozen
            if current_index in layers_to_freeze:
                for param in params:
                    param.requires_grad = False
                current_index += 1

def _create_classifier(classifier_layer_sizes, hidden_activation, final_activation, dropouts):
    layers = []
    FIRST_SIZE = 512 * 7 * 7 # constant for VGG16
    LAST_SIZE = 2 # constant for binary classification
    for idx, layer_size in enumerate(classifier_layer_sizes):
        if idx == 0:
            new_layer = nn.Linear(FIRST_SIZE, layer_size)
            new_activation = hidden_activation()
        else:
            new_layer = nn.Linear(classifier_layer_sizes[idx - 1], layer_size)
            new_activation = hidden_activation()
        layers.append(new_layer)
        layers.append(new_activation)
        if dropouts and len(dropouts) > idx:
            if dropouts[idx]:
                layers.append(nn.Dropout(dropouts[idx]))
    if len(classifier_layer_sizes) == 0:
        layer_size = FIRST_SIZE
        idx = -1
    final_layer = nn.Linear(layer_size, LAST_SIZE)
    final_activation = final_activation()
    layers.append(final_layer)
    layers.append(final_activation)
    if dropouts and len(dropouts) > idx + 1:
        if dropouts[idx + 1]:
            layers.append(nn.Dropout(dropouts[idx]))
    return nn.Sequential(*layers)

def create_model(classifier_layer_sizes,
                 hidden_activation, final_activation,
                 batch_norm, freeze_layers=None, freeze_below=True,
                 dropouts=None, pretrained=True):
    if batch_norm:
        model = vgg16_bn(pretrained=pretrained)
    else:
        model = vgg16(pretrained=pretrained)
    # we can overwrite the default model.classifier because it's easily
    # accessible as an attribute
    model.classifier = _create_classifier(classifier_layer_sizes,
                                          hidden_activation,
                                          final_activation, dropouts)
    if freeze_layers:
        if freeze_below:
            layers_to_freeze = range(0, freeze_layers)
        else:
            num = _num_trainable_layers(model)
            layers_to_freeze = range(freeze_layers, num + 1)
        _freeze_layers(model, layers_to_freeze)
    return model
