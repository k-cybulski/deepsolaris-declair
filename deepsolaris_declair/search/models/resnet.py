from collections import namedtuple

from torch import nn
from torchvision.models import resnet50

# Freezing layers in pytorch resnets is a bit complicated. Biases and
# weights are not stored in the same pytorch module, but in two - Conv2d
# for weights and BatchNorm2d for biases. So, we make a helper type which we
# can freeze.

class _ResnetLayer:
    def __init__(self, conv, batchnorm):
        self.conv = conv
        self.batchnorm = batchnorm
    def freeze(self):
        for p in self.conv.parameters():
            p.requires_grad = False
        for p in self.batchnorm.parameters():
            p.requires_grad = False

def _unroll_to_layers(model):
    """Creates a list of conv-batchnorm pairs of the model in order
    of execution"""
    out_list = []
    conv = None
    for child in model.children():
        if len(list(child.children())) > 0:
            out_list.extend(_unroll_to_layers(child))
        elif isinstance(child, nn.BatchNorm2d) and conv is not None:
            batchnorm = child
            layer = _ResnetLayer(conv, batchnorm)
            out_list.append(layer)
            conv = None
        elif isinstance(child, nn.Conv2d):
            conv = child
    return out_list

def _freeze_layers(model, layers_to_freeze):
    for idx, layer in enumerate(_unroll_to_layers(model)):
        if idx in layers_to_freeze:
            layer.freeze()

def create_model(final_activation, resnet_type=resnet50, freeze_layers=None, pretrained=True):
    LAST_SIZE = 2 # constant for binary classification
    model = resnet_type(pretrained=pretrained)
    # Modify the final classifier
    model.fc = nn.Sequential(
        nn.Linear(model.fc.in_features, LAST_SIZE, bias=True),
        final_activation())
    if freeze_layers:
        layers_to_freeze = range(0, freeze_layers)
        _freeze_layers(model, layers_to_freeze)
    return model
