"""
Very simple models for testing.
"""
from torch import nn

from torchvision import models
def naive_sigmoid():
    """
    A very simple binary classifier based on VGG16.
    """
    vgg16 = models.vgg16(pretrained=True)
    vgg16.classifier = nn.Sequential(
            nn.Linear(512 * 7 * 7, 500),
            nn.Sigmoid(),
            nn.Linear(500, 2),
            nn.Softmax()
        )
    return vgg16
