import numpy as np
from ignite.engine import Events
from ignite.metrics import Loss

from declair.frameworks.ignite.evaluators import EngineOutputTracker

def get_optimal_threshold(y_true, y_pred, func):
    """Returns the optimal threshold for a given classification
    metric function `func`.
    """
    candidates = np.linspace(0, 1)
    threshold_func = lambda x: (y_pred > x) * 1
    thresholded_preds = map(threshold_func, candidates)

    vals = list(map(lambda y_pred_thresholded: func(y_pred_thresholded, y_true),
               thresholded_preds))
    optimal_threshold = candidates[np.argmax(vals)]
    return optimal_threshold

def compute_ignite_metric(y_true, y_pred_thresholded,
                          metric_type, metric_params={}):
    """Returns the value of a PyTorch Ignite metric given true
    labels and model predictions.
    """
    metric = metric_type(**metric_params)
    expected_output = (y_pred_thresholded, y_true)
    metric.update(expected_output)
    return metric.compute()

def get_optimal_threshold_ignite_metric(y_true, y_pred,
                                        metric_type, metric_params={}):
    opt_func = lambda y_pred, y: compute_ignite_metric(
        y_pred, y, metric_type, metric_params)
    opt_threshold = get_optimal_threshold(y_true, y_pred, opt_func)
    return opt_threshold

class ThresholdTransformer:
    """A class to transform model outputs into a form parsed
    by PyTorch Ignite thresholded classification metrics, with
    an adjustable threshold.
    """
    def __init__(self, threshold, output_transform=lambda x: x):
        self._threshold = threshold
        self._output_transform = output_transform

    def set(self, threshold):
        self._threshold = threshold

    def __call__(self, output):
        y_pred = output[0]
        y = output[1]
        y_pred_thresholded = (y_pred > self._threshold) * 1.
        return y_pred_thresholded, y

thresholdless = [
    Loss
]

def create_optimal_threshold_report(metric_calls, sacred_output_saver):
    out_str = ""
    for saver_name, saver in sacred_output_saver.get_savers().items():
        y_pred_two_columns, y = saver.get_output()
        y_pred = y_pred_two_columns[:, 1]
        out_str += """Engine {}:
metric_name:\toptimal_threshold,\toptimal_value
""".format(saver_name)
        for name, call_dict in metric_calls.items():
            metric_type = call_dict['metric']
            if metric_type in thresholdless:
                continue
            metric_params = call_dict.get('kwargs', {})
            optimal_threshold = get_optimal_threshold_ignite_metric(
                y, y_pred, metric_type, metric_params=metric_params)
            y_opt = (y_pred > optimal_threshold) * 1.
            metric_value = compute_ignite_metric(
                y, y_opt, metric_type, metric_params=metric_params)
            out_str += "{}:\t{:.3f},\t{:.3f}\n".format(
                name, optimal_threshold, metric_value)
        out_str += "\n"
    return out_str
