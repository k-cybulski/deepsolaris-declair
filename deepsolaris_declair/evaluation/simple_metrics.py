from ignite.metrics import Metric
from ignite.exceptions import NotComputableError

class OutputLoss(Metric):
    def __init__(self, output_transform=lambda x: x):
        self._total_loss = None
        self._num_samples = None
        super(OutputLoss, self).__init__(output_transform=output_transform)

    def reset(self):
        self._total_loss = 0
        self._num_samples = 0
        super(OutputLoss, self).reset()

    def update(self, output):
        x = output[0]
        loss = output[-1]

        self._total_loss += loss
        self._num_samples += x.shape[0]

    def compute(self):
        if self._num_samples == None:
            raise NotComputableError('OutputLoss must have at least one example before it can be computed.')
        return self._total_loss/self._num_samples
