import torch
from ignite.metrics import Metric

class _ConfusionMatrixMetric(Metric):
    """Generic metric based on the confusion matrix."""
    def __init__(self, output_transform=lambda x: x):
        self.tp = None
        self.tn = None
        self.fp = None
        self.fn = None
        super(_ConfusionMatrixMetric, self).__init__(output_transform=output_transform)

    def reset(self):
        self.tp = None
        self.tn = None
        self.fp = None
        self.fn = None
        super(_ConfusionMatrixMetric, self).reset()

class ClassBalanceAccuracy(_ConfusionMatrixMetric):
    """Class balance accuracy based on Luque et al.(2019)"""
    def update(self, output):
        y_pred_raw, y = output
        if any(x is None for x in [self.tp, self.tn, self.fp, self.fn]):
            # need to define torch float tensors instead of just 0
            # because we want to have control over its dtype
            # if we don't make sure the dtype is a floating point number
            # then the computations in `compute` will be floored to 0
            self.tp = torch.tensor(0, dtype=torch.float, device=y.device)
            self.tn = torch.tensor(0, dtype=torch.float, device=y.device)
            self.fp = torch.tensor(0, dtype=torch.float, device=y.device)
            self.fn = torch.tensor(0, dtype=torch.float, device=y.device)
        if y_pred_raw.ndimension() > y.ndimension():
            y_pred = torch.argmax(y_pred_raw, dim=1).to(torch.bool)
        else:
            y_pred = y_pred_raw.to(torch.bool)
        y_as_true = y[y_pred]
        self.tp += torch.sum(y_as_true)
        self.fp += torch.numel(y_as_true) - torch.sum(y_as_true)
        y_as_false = y[~y_pred]
        self.tn += torch.numel(y_as_false) - torch.sum(y_as_false)
        self.fn += torch.sum(y_as_false)

    def compute(self):
        lambda_pp = self.tp / (self.tp + self.fn)
        lambda_nn = self.tn / (self.tn + self.fp)
        out = (lambda_pp + lambda_nn) / 2
        return out

