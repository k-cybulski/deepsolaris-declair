import torch
from ignite.metrics import Metric
from ignite.exceptions import NotComputableError

def segmentation_decision(batch_output, threshold=0.5):
    """Transforms a tensor of binary softmax probabilities into a tensor of
    decisions, where 1 means "positive case"."""
    return (batch_output[:,1] > threshold).to(torch.float)

class IntersectionOverUnion(Metric):
    def __init__(self, output_transform=lambda x: x):
        self._total_intersection = None
        self._total_union = None
        super(IntersectionOverUnion, self).__init__(output_transform=output_transform)

    def reset(self):
        self._total_intersection = 0
        self._total_union = 0
        super(IntersectionOverUnion, self).reset()

    def update(self, output):
        y, y_pred = output
        y_decisions = segmentation_decision(y_pred)
        self._total_intersection += torch.logical_and(y, y_decisions).sum()
        self._total_union += torch.logical_or(y, y_decisions).sum()

    def compute(self):
        if self._total_union is None:
            raise NotComputableError('IntersectionOverUnion must have at least one example before it can be computed.')
        return self._total_intersection / self._total_union
