from tempfile import NamedTemporaryFile

import torch
import numpy as np
from matplotlib import pyplot as plt

from declair.results import add_artifact_fig

def _acc(y_pred, y):
    correct = y_pred == y
    return torch.true_divide(torch.sum(correct), y.numel())

def _cb_acc(y_pred, y):
    y_as_true = y[y_pred]
    tp = torch.sum(y_as_true)
    fp = torch.numel(y_as_true) - torch.sum(y_as_true)
    y_as_false = y[~y_pred]
    tn = torch.numel(y_as_false) - torch.sum(y_as_false)
    fn = torch.sum(y_as_false)

    lambda_pp = tp / (tp + fn)
    lambda_nn = tn / (tn + fp)
    out = (lambda_pp + lambda_nn) / 2
    return out

def _cr(softmax_response, y_pred, y, threshold):
    acceptance_mask = softmax_response > threshold
    coverage = torch.true_divide(torch.sum(acceptance_mask), torch.numel(acceptance_mask))
    accepted_y_pred = y_pred[acceptance_mask]
    accepted_y = y[acceptance_mask]
    risk = 1 - _acc(accepted_y_pred, accepted_y)
    return coverage, risk

def _compute_cr_curve(y_pred_two_column, y):
    softmax_response, y_pred = torch.max(y_pred_two_column, dim=1)
    thresholds = np.linspace(0.5, 1, num=100)
    risks = []
    coverages = []
    for t in thresholds:
        risk, coverage = _cr(
            softmax_response, y_pred, y, t)
        if torch.isnan(risk) or torch.isnan(risk):
            break
        risks.append(risk)
        coverages.append(coverage)
    return risks, coverages

def _plot_cr_curve(plt, y_pred_two_column, y):
    coverages, risks = _compute_cr_curve(y_pred_two_column, y)
    plt.plot(coverages, risks)
    plt.suptitle("Coverage-Risk curve")
    plt.ylabel("Risk")
    plt.xlabel("Coverage")

def plot_and_save_cr_curve(run, sacred_output_saver):
    for name, saver in sacred_output_saver._savers.items():
        y_pred_two_column, y = saver.get_output()
        _plot_cr_curve(plt, y_pred_two_column, y)
        add_artifact_fig(run, plt, '{}_coverage_risk_plot.png'.format(name))
        plt.close()
