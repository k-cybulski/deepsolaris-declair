from ..transforms import standard_norm_undo_func

def fig_tensor(tensor, subplots_shape, plt, title, size=(5, 5), denormalize=False):
    """Returns a figure with images from a tensor tensor visualized.

    Args:
        tensor: A tensor of images
        subplots_shape: A tuple of integers that defines the number of rows and columns
        plt:    Imported matplotlib.pyplot module
        title:  Title of the figure
        size:   Tuple defining size of the image
        denormalize:    Whether or not the input tensor is normalized
    """
    num_of_subplots = abs(subplots_shape[0] * subplots_shape[1])
    tensor = tensor[:num_of_subplots].cpu().detach()
    if denormalize:
        tensor = standard_norm_undo_func(tensor.clone())
    fig, ax_ndarray = plt.subplots(*subplots_shape, gridspec_kw={'left':0.05, 'right':0.95, 'bottom': 0.05, 'top':0.95, 'hspace': 0.05, 'wspace': 0.05})
    for ax in ax_ndarray.flatten():
        ax.tick_params(
            axis='both',   
            which='both',  
            bottom=False,  
            top=False,     
            left=False,
            right=False,
            labelbottom=False,
            labelleft=False)
    for ax, img in zip(ax_ndarray.flatten(), tensor):
        ax.imshow(img.T)
    fig.suptitle(title)
    return fig

def fig_triplets(tensor_x, tensor_y, tensor_y_pred, plt, title, num=None, size=None, denormalize=False):
    if num is None:
        num = min(
            len(tensor_x),
            len(tensor_y),
            len(tensor_y_pred)
        )
    if size is None:
        size = (3 * num, 3 * 3)
    x = tensor_x[:num].cpu().detach()
    y = tensor_y[:num].cpu().detach()
    y_pred = tensor_y_pred[:num].cpu().detach()
    if denormalize is True:
        x = standard_norm_undo_func(x.clone())
        y = standard_norm_undo_func(y.clone())
        y_pred = standard_norm_undo_func(y_pred.clone())
    elif denormalize is not False:
        # if it's a list of 'indices' referring to the first, second or third tensor
        if 0 in denormalize:
            x = standard_norm_undo_func(x.clone())
        if 1 in denormalize:
            y = standard_norm_undo_func(y.clone())
        if 2 in denormalize:
            y_pred = standard_norm_undo_func(y_pred.clone())
    fig, ax_ndarray = plt.subplots(3, num, gridspec_kw={'left':0.05, 'right':0.95, 'bottom': 0.05, 'top':0.95, 'hspace': 0.05, 'wspace': 0.05})
    row_titles = ['Input', 'Target', 'Prediction']
    for ax, row in zip(ax_ndarray[:,0], row_titles):
        ax.set_ylabel(row, rotation=90, size='large')
    for ax in ax_ndarray.flatten():
        ax.tick_params(
            axis='both',   
            which='both',  
            bottom=False,  
            top=False,     
            left=False,
            right=False,
            labelbottom=False,
            labelleft=False)
    for i in range(num):
        ax_ndarray[0][i].imshow(x[i].T)
        ax_ndarray[1][i].imshow(y[i].T)
        ax_ndarray[2][i].imshow(y_pred[i].T)
    fig.suptitle(title)
    fig.set_size_inches(*size)
    return fig
