import torch
from matplotlib import pyplot as plt
from sklearn.calibration import calibration_curve

from declair.results import add_artifact_fig

def _plot_calibration_curve(plt, y_pred_two_column, y):
    y_pred = y_pred_two_column[:, 1].cpu().detach()
    y_pred = torch.clamp(y_pred, 0, 1).numpy()
    y = y.cpu().detach().numpy()
    prob_true, prob_pred = calibration_curve(
        y, y_pred, n_bins=50, strategy='quantile')

    fig, ax = plt.subplots()
    ax.set_xlim(left=0, right=1)
    ax2 = plt.twinx(ax)
    ax.hist(y_pred, bins=10, color='xkcd:celadon', range=(0, 1))
    ax2.plot([0, 1], [0, 1], linestyle='--')
    ax2.plot(prob_pred, prob_true)

    fig.suptitle("Calibration curve")
    ax.set_ylabel("Number of samples")
    ax2.set_ylabel("Empirical probability")
    ax.set_xlabel("Predicted probability")

def plot_and_save_calibration_curve(run, sacred_output_saver):
    for name, saver in sacred_output_saver._savers.items():
        y_pred_two_column, y = saver.get_output()
        _plot_calibration_curve(plt, y_pred_two_column, y)
        add_artifact_fig(run, plt, '{}_calibration_curve_plot.png'.format(name))
        plt.close()
