from ignite.engine import create_supervised_trainer, create_supervised_evaluator, Events
from ignite.metrics import Accuracy, Precision, Recall, Loss
from ignite.handlers import EarlyStopping
from torchsummary import summary

from declair import manual
from declair.results import add_artifact_string
from declair.frameworks.ignite.evaluators import (attach_metrics,
                                                  SacredMetricsLogger,
                                                  SacredOutputSaver)
from declair.frameworks.ignite.gpu import clear_device_cache

from .evaluation.class_balance import ClassBalanceAccuracy
from .evaluation.optimal_threshold import create_optimal_threshold_report
from .evaluation.selective_classification import plot_and_save_cr_curve
from .evaluation.calibration_curve import plot_and_save_calibration_curve
from .data import loaders_summary

def execute(params, _run):
    model = params['architecture']['model'].to(params['device'])
    criterion = params['architecture']['criterion']
    optimizer = manual(params['optimizer'], model.parameters())

    train_loader, validation_loader, test_loader = params['dataset']
    _, _, extra_test_loader = params['extra_dataset']

    max_epochs = params['max_epochs']

    trainer_output_transform = lambda x, y, y_pred, loss: (y_pred, y)

    train_engine = create_supervised_trainer(model, optimizer, criterion,
                                             output_transform=trainer_output_transform)
    val_engine = create_supervised_evaluator(model)
    test_engine = create_supervised_evaluator(model)
    extra_test_engine = create_supervised_evaluator(model)

    # Run the model on the validation set after every training epoch.
    @train_engine.on(Events.EPOCH_COMPLETED(every=1))
    def run_val_engine(engine):
        val_engine.run(validation_loader)

    # Run the model on the test set at the very end of training.
    @train_engine.on(Events.COMPLETED)
    def run_test_engine(engine):
        test_engine.run(test_loader)

    # Run the model on the extra dataset at the end of training
    @train_engine.on(Events.COMPLETED)
    def run_extra_test_engine(engine):
        extra_test_engine.run(extra_test_loader)

    
    def metrics_output_threshold(output):
        y_pred, y = output
        return (y_pred > 0.5)[:, 1] * 1., y
    
    metrics_calls = {
        'accuracy': {"metric": Accuracy,
                     "kwargs": {"output_transform": metrics_output_threshold}},
            'precision': {"metric": Precision,
                          "kwargs": {"output_transform": metrics_output_threshold}},
            'recall': {"metric": Recall,
                       "kwargs": {"output_transform": metrics_output_threshold}},
            'cb_accuracy': {"metric": ClassBalanceAccuracy,
                            "kwargs": {
                                "output_transform": metrics_output_threshold}
                            },
            'loss': {"metric": Loss,
                     "kwargs": {"loss_fn": criterion}}
        }
    output_saver = SacredOutputSaver(clear_on_completion=False)

    attach_metrics(_run,
                   [train_engine, val_engine, test_engine, extra_test_engine],
                   metrics_calls)
    evaluators = [
        SacredMetricsLogger(),
        output_saver
    ]

    for evaluator in evaluators:
        evaluator.attach(_run, ['train', 'val', 'test', 'extra_test'],
                         train_engine, val_engine, test_engine, extra_test_engine)

    # Early stopping
    def early_stopping_score_function(engine):
        val_loss = engine.state.metrics['loss']
        return -val_loss
    stopper_handler = EarlyStopping(patience=3,
                                    score_function=early_stopping_score_function,
                                    trainer=train_engine)
    val_engine.add_event_handler(Events.COMPLETED, stopper_handler)

    add_artifact_string(_run,
                        str(summary(model, (3, 187, 187), device=params['device'])),
                        'model_summary')
    add_artifact_string(_run,
                        loaders_summary(train_loader, validation_loader, test_loader, extra_test_loader),
                        'data_summary')

    train_engine.run(train_loader, max_epochs=max_epochs)

    optimal_threshold_report = create_optimal_threshold_report(metrics_calls, output_saver)
    add_artifact_string(_run, optimal_threshold_report, 'optimal_threshold_report')
    plot_and_save_cr_curve(_run, output_saver)
    plot_and_save_calibration_curve(_run, output_saver)
    # Output metrics at the end for hyperopt optimization search.
    # The hyperopt optimizer needs to pick a single target metric to optimize.
    return {
        **{'train_{}'.format(key): value for key, value in train_engine.state.metrics.items()},
        **{'val_{}'.format(key): value for key, value in val_engine.state.metrics.items()},
        **{'test_{}'.format(key): value for key, value in test_engine.state.metrics.items()},
        **{'extra_test_{}'.format(key): value for key, value in test_engine.state.metrics.items()}
    }

def cleanup(params):
    device = params['device']
    clear_device_cache(device)
