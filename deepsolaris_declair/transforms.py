from torchvision.transforms import Normalize
import torch

# Pretrained torchvision models assume this normalization
_STANDARD_NORM_MEAN = [0.485, 0.456, 0.406]
_STANDARD_NORM_STD =  [0.229, 0.224, 0.225]

def standard_norm(inplace=False):
    """A normalization transform standard for pretrained models."""
    # Maybe this should have inplace=True for space efficiency
    return Normalize(mean=_STANDARD_NORM_MEAN,
                     std=_STANDARD_NORM_STD, inplace=inplace)

def _norm_undo_func(tensor, mean, std, inplace=False):
    # Reverses standard_norm
    # made to undo torchvision.transforms.functional.normalize
    if not inplace:
        tensor = tensor.clone()
    dtype = tensor.dtype
    mean = torch.as_tensor(mean, dtype=dtype, device=tensor.device)
    std = torch.as_tensor(std, dtype=dtype, device=tensor.device)
    if mean.ndim == 1:
        mean = mean.view(-1, 1, 1)
    if std.ndim == 1:
        std = std.view(-1, 1, 1)
    tensor.mul_(std).add_(mean)
    return tensor

def standard_norm_undo_func(tensor, inplace=False):
    return _norm_undo_func(tensor, _STANDARD_NORM_MEAN, _STANDARD_NORM_STD,
                           inplace=inplace)

class NormalizeUndo(Normalize):
    """A reverse transform for torch.transforms.Normalize."""
    def forward(self, tensor: torch.Tensor) -> torch.Tensor:
        return _norm_undo_func(tensor, self.mean, self.std, self.inplace)

def standard_norm_undo(inplace=False):
    """A reverse transform for standard normalization."""
    return NormalizeUndo(mean=_STANDARD_NORM_MEAN,
                         std=_STANDARD_NORM_STD, inplace=inplace)

class GaussianNoise(torch.nn.Module):
    def __init__(self, std):
        super().__init__()
        self._std = std

    def forward(self, img, inplace=False):
        if not inplace:
            img = img.clone()
        noise = torch.normal(0, self._std, size=img.shape)
        img += noise
        return img
