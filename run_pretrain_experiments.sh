
echo "Running segmentation_random"
declair-execute ./experiments/domain_adaptation/pretraining/pretrain_segmentation_random_vgg11.json
declair-execute ./experiments/domain_adaptation/pretraining/pretrain_segmentation_random_vgg16.json

echo "Running segmentation_imagenet"
declair-execute ./experiments/domain_adaptation/pretraining/pretrain_segmentation_imagenet_vgg11.json
declair-execute ./experiments/domain_adaptation/pretraining/pretrain_segmentation_imagenet_vgg16.json

echo "Running reconstruction"
declair-execute ./experiments/domain_adaptation/pretraining/pretrain_reconstruction_random_vgg16.json
declair-execute ./experiments/domain_adaptation/pretraining/pretrain_reconstruction_imagenet_vgg16.json
