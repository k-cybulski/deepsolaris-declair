# Creates a temporary yaml config file which combines environment from the root
# of this repo with _vastai_declair_env.yaml and returns its name
from pathlib import Path
from tempfile import NamedTemporaryFile
from declair.env import get_repo_env

base = get_repo_env()
base.update_from_file(Path(__file__).with_name('_vastai_declair_env.yaml'))
# fetch a temporary file name
with NamedTemporaryFile() as f:
    filename = f.name
base.dump(filename)
print(filename)
