#!/bin/bash
# This script assumes we are dealing with with an ssh-accessible vastai
# instance that has the image pytorch/pytorch:1.4-cuda10.1-cudnn7-runtime (or
# similar)
set -e

if [ ! -f requirements.txt ]; then
    echo "requirements.txt not found. Are you sure you're in the root project directory?"
    exit 1
fi

if [ -n "$1" ] ; then
    export VASTAI_PORT=$1
fi

if [ -n "$2" ] ; then
    export VASTAI_HOSTNAME=$2
fi

if [ -z "$VASTAI_PORT" ] ; then
    echo "Please provide the port"
    exit 1
fi

if [ -z "$VASTAI_HOSTNAME" ] ; then
    echo "Please provide the hostname"
    exit 1
fi


echo "Note that the machine is not guaranteed to be secure, hence it should not be used for confidential data."
echo -n "Is the data open (y/n)? "
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo "Proceeding to setup"
else
    echo "Aborting"
    exit 1
fi

DATA_DIR=$(python -c "from declair.env import get_repo_env
print(get_repo_env()['dataset']['dir'])
")

ENV_FILE=$(python vastai/_create_env.py)

echo "Installing rsync on the server..."
ssh -i $HOME/.ssh/vastai -p "$VASTAI_PORT" "$VASTAI_HOSTNAME" "apt-get install -y rsync"
echo "Sending data..."
rsync --partial --append -r --progress --compress --copy-links -e "ssh -i $HOME/.ssh/vastai -p \"$VASTAI_PORT\"" "$DATA_DIR/" "$VASTAI_HOSTNAME":~/data
echo "Sending code..."
rsync --partial --append -r --progress --compress --copy-links --exclude="*.venv*" -e "ssh -i $HOME/.ssh/vastai -p \"$VASTAI_PORT\"" "./" "$VASTAI_HOSTNAME":~/
echo "Uploading environment config..."
scp -i $HOME/.ssh/vastai -P "$VASTAI_PORT" "$ENV_FILE" $VASTAI_HOSTNAME:/root/declair_env.yaml
rm "$ENV_FILE"
echo "Installing packages..."
ssh -i $HOME/.ssh/vastai -p "$VASTAI_PORT" "$VASTAI_HOSTNAME" "/opt/conda/bin/pip install --ignore-installed -r requirements.txt"
echo "Running tests..."
ssh -i $HOME/.ssh/vastai -p "$VASTAI_PORT" "$VASTAI_HOSTNAME" "/opt/conda/bin/python -m pytest tests"
