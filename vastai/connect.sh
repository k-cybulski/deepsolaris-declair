#!/bin/bash
# Connects via SSH to the container. Port, username and hostname are found
# under the Instances tab after clicking "Connect"
# Sample usage:
#  connect.sh 30677 root@ssh4.vast.ai

if [ -n "$1" ] ; then
    export VASTAI_PORT=$1
fi

if [ -n "$2" ] ; then
    export VASTAI_HOSTNAME=$2
fi

if [ -z "$VASTAI_PORT" ] ; then
    echo "Please provide the port"
    exit 1
fi

if [ -z "$VASTAI_HOSTNAME" ] ; then
    echo "Please provide the hostname"
    exit 1
fi

ssh -i $HOME/.ssh/vastai -p "$VASTAI_PORT" "$VASTAI_HOSTNAME" -L 8080:localhost:8080
