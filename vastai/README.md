# Vast.ai
The code in this repository was designed to be easily uploadable onto an external GPU machine, specifically vast.ai. This directory contains scripts that automate the process.

## Main idea
Vast.ai is a GPU sharing service and it operates on the basis of users hosting their machines' compute power cheaply via docker containers. Containers may be reached either via Jupyter or via SSH, and it is the latter that allows for easy code reuse between instances.

If SSH connection is selected, the container is accessible under a port and hostname given in the _Instances_ tab after clicking _connect_. It is necessary to set up an ssh key locally for vast.ai in order to be able to connect.

## Container config
The container this code has been tried on is `pytorch/pytorch:1.4-cuda10.1-cudnn7-runtime`, with launch type set to SSH.
