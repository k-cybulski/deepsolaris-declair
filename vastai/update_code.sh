#!/bin/bash

if [ -n "$1" ] ; then
    export VASTAI_PORT=$1
fi

if [ -n "$2" ] ; then
    export VASTAI_HOSTNAME=$2
fi

if [ -z "$VASTAI_PORT" ] ; then
    echo "Please provide the port"
    exit 1
fi

if [ -z "$VASTAI_HOSTNAME" ] ; then
    echo "Please provide the hostname"
    exit 1
fi

ENV_FILE=$(python vastai/_create_env.py)

rsync -vr --progress --compress --copy-links --exclude="*.venv*" -e "ssh -i $HOME/.ssh/vastai -p \"$VASTAI_PORT\"" "./" "$VASTAI_HOSTNAME":~/
scp -i $HOME/.ssh/vastai -P "$VASTAI_PORT" "$ENV_FILE" $VASTAI_HOSTNAME:/root/declair_env.yaml
rm "$ENV_FILE"
ssh -i $HOME/.ssh/vastai -p "$VASTAI_PORT" "$VASTAI_HOSTNAME" "/opt/conda/bin/pip install --ignore-installed -Ur requirements.txt"
