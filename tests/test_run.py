"""
These tests simply run experiments on smaller datasets to see if the
architectures crash.
"""

from shutil import rmtree
from tempfile import mkdtemp

import torch
import pytest

from declair.execution import execute_file
from declair.env import Environment

def test_run():
    tmpdir = mkdtemp()
    env = Environment({
        'observers': {'file': {'path': tmpdir}},
        'dataset': {'dir': "./tests/data/sample.npy"},
        'device': 'cpu'
    })
    execute_file('./tests/test_run_definition.yaml', env=env)
    rmtree(tmpdir)

def test_gpu_run():
    if not torch.cuda.is_available():
        pytest.skip("GPU unavailable for testing")
    tmpdir = mkdtemp()
    env = Environment({
        'observers': {'file': {'path': tmpdir}},
        'dataset': {'dir': "./tests/data/sample.npy"},
        'device': 'cuda'
    })
    execute_file('./tests/test_run_definition.yaml', env=env)
    rmtree(tmpdir)


def test_search():
    tmpdir = mkdtemp()
    env = Environment({
        'observers': {'file': {'path': tmpdir}},
        'dataset': {'dir': "./tests/data/sample.npy"},
        'device': 'cpu'
    })
    execute_file('./tests/test_model_search.yaml', env=env)
    rmtree(tmpdir)
