import pytest
import numpy as np
import torch
from torchvision.transforms import ToPILImage
from torch.utils.data import DataLoader

from deepsolaris_declair.data import _load_dataset_numpy, DeepSolarisDatasetNumpy

@pytest.fixture
def data_specs():
    return 'sample', './tests/data'

def test_load_data(data_specs):
    name, data_dir = data_specs
    images, labels = _load_dataset_numpy(name, data_dir)
    assert images.shape == (12, 187, 187, 3)

@pytest.fixture
def sample_data(data_specs):
    return _load_dataset_numpy(*data_specs)

def test_DeepSolarisDatasetNumpy(data_specs):
    dataset = DeepSolarisDatasetNumpy(*data_specs, normalize=False)
    for image, label in dataset:
        assert torch.max(image) <= 1.
        assert torch.min(image) >= 0
