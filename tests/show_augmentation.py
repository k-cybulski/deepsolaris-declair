"""
Short script that presents sample images from dataset.
"""
from torchvision.transforms import Compose, ToTensor, ToPILImage
from torchvision import transforms as t
import numpy as np
from matplotlib import pyplot as plt

from deepsolaris_declair.data import DeepSolarisDatasetNumpy

DATASET_DIR = './tests/data'
TEST_DATASET_NAME = 'sample'

configurations = [
    ('None',
     {
            'transform': Compose([
            ])
     }),
    ('Random rotation',
     {
            'transform': Compose([
                ToPILImage(),
                t.RandomRotation(180),
                ToTensor()
            ])
     }),
    ('Color jitter',
     {
            'transform': Compose([
                ToPILImage(),
                t.ColorJitter(0.5, 0.5, 0.5),
                ToTensor()
            ])
     }),
    ('Both',
     {
            'transform': Compose([
                ToPILImage(),
                t.RandomRotation(180),
                t.ColorJitter(0.5, 0.5, 0.5),
                ToTensor()
            ])
     })
]

for config_name, configuration in configurations:
    dataset = DeepSolarisDatasetNumpy(TEST_DATASET_NAME, DATASET_DIR, 
                                 transform=configuration['transform'], normalize=False)
    images = [image for image, label in dataset]
    fig, lists = plt.subplots(3, 4)
    flat_axes = []
    for l in lists:
        for ax in l:
            flat_axes.append(ax)
    for ax, img in zip(flat_axes, images):
        ax.tick_params(
            axis='both',   
            which='both',  
            bottom=False,  
            top=False,     
            left=False,
            right=False,
            labelbottom=False,
            labelleft=False)
        ax.imshow(img.T)
    fig.suptitle(config_name)
    plt.show()
