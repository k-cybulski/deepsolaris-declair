import pytest
import torch
from sklearn import metrics as sklearn_metrics
from ignite import metrics as ignite_metrics
from ignite.engine import Engine

from declair.frameworks.ignite.evaluators import EngineOutputTracker

from deepsolaris_declair.evaluation.optimal_threshold import (
    compute_ignite_metric, ThresholdTransformer,
    get_optimal_threshold,
    get_optimal_threshold_ignite_metric)

@pytest.fixture
def sample_predictions():
    y_pred = torch.tensor([0.2, 0.3, 0.1, 0.8, 0.9, 0.86, 0.7, 0.66, 0.99])
    y = torch.tensor([0, 1, 0, 1, 1, 0, 0, 0, 1])
    return y_pred, y

def test_threshold_transformer(sample_predictions):
    trans = ThresholdTransformer(0.35)
    cases = [
        (0.0, torch.ones_like(sample_predictions[0])),
        (0.35, torch.tensor([0., 0., 0., 1., 1., 1., 1., 1., 1.])),
        (1.0, torch.zeros_like(sample_predictions[0]))
    ]
    for threshold, expected_y_pred_thresholded in cases:
        trans.set(threshold)
        assert all(trans(sample_predictions)[0] == expected_y_pred_thresholded)

@pytest.fixture
def default_thresholder():
    return ThresholdTransformer(0.5)

def test_compute_ignite_metric(sample_predictions, default_thresholder):
    y_pred, y_true = default_thresholder(sample_predictions)
    cases = [
        (sklearn_metrics.accuracy_score, ignite_metrics.Accuracy),
        (sklearn_metrics.precision_score, ignite_metrics.Precision),
        (sklearn_metrics.recall_score, ignite_metrics.Recall)
    ]
    for sk_metric, ig_metric in cases:
        assert (sk_metric(y_true, y_pred)
                == compute_ignite_metric(y_true, y_pred, ig_metric))

def test_optimal_threshold_equivalence(sample_predictions):
    y_pred, y_true = sample_predictions
    assert (get_optimal_threshold(
        y_true, y_pred, sklearn_metrics.accuracy_score)
            == get_optimal_threshold_ignite_metric(
        y_true, y_pred, ignite_metrics.Accuracy))

@pytest.fixture
def sample_engine(sample_predictions):
    process_function = lambda x, y: sample_predictions
    engine = Engine(process_function)
    return engine

def test_threshold_transformer_as_transform_of_ignite_metric(sample_predictions, sample_engine):
    transformer = ThresholdTransformer(0.5)
    acc_metric = ignite_metrics.Accuracy(transformer)
    acc_metric.attach(sample_engine, 'accuracy')
    cases = [0.35, 0.5, 0.75]
    for t in cases:
        transformer.set(t)
        sample_engine.run([[1]])
        y_pred, y_true = transformer(sample_predictions)
        assert (sklearn_metrics.accuracy_score(y_true, y_pred) 
                == sample_engine.state.metrics['accuracy'])

def test_threshold_setting(sample_predictions, sample_engine):
    thresholder = ThresholdTransformer(0.5)
    output_tracker = EngineOutputTracker()
    output_tracker.attach_engine(sample_engine)
    metric_acc = ignite_metrics.Accuracy(thresholder)
    metric_acc.attach(sample_engine, 'accuracy')

    sample_engine.run([[1]])
    y_pred, y_true = output_tracker.get_output()
    thresholder.set(
        get_optimal_threshold_ignite_metric(y_true, y_pred,
                                            ignite_metrics.Accuracy))
    y_pred, y_true = sample_predictions
    opt_threshold = get_optimal_threshold(
        y_true, y_pred, sklearn_metrics.accuracy_score)

    sample_engine.run([[1]]) # re-compute accuracy with the new threshold
    y_pred_new, y_true = ThresholdTransformer(opt_threshold)(sample_predictions)
    assert (sample_engine.state.metrics['accuracy'] 
            == sklearn_metrics.accuracy_score(y_true, y_pred_new))
