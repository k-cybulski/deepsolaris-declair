import pytest
from torch.utils.data import DataLoader

from deepsolaris_declair.data import DeepSolarisDatasetNumpy
from deepsolaris_declair.transforms import standard_norm, standard_norm_undo

@pytest.fixture
def sample_batch_classification():
    sample_dataset = DeepSolarisDatasetNumpy('sample', './tests/data', normalize=False)
    loader = DataLoader(sample_dataset, 2)
    for batch in loader:
        break
    return batch

def test_standard_norm_and_undo_as_approximate_inverse(sample_batch_classification):
    img = sample_batch_classification[0][0]
    norm = standard_norm()
    denorm = standard_norm_undo()
    assert (denorm(norm(img)) - img).abs().max() < 10**(-5)
