# DeepSolaris Declair
[![pipeline status](https://gitlab.com/k-cybulski/deepsolaris-declair/badges/master/pipeline.svg)](https://gitlab.com/k-cybulski/deepsolaris-declair/-/commits/master)
[![coverage report](https://gitlab.com/k-cybulski/deepsolaris-declair/badges/master/coverage.svg)](https://gitlab.com/k-cybulski/deepsolaris-declair/-/commits/master)
Code to recreate DeepSolaris models using Declair.
