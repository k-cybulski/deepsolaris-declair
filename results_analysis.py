#!/usr/bin/env python3
"""
This script is for quickly skimming through results stored with the Sacred file observer.
"""

import argparse
import os
import json
from dateutil.parser import isoparse

from tabulate import tabulate
from declair import Environment
from declair.env import get_path_env
from declair.const import (DEF_STORED_RUN_CONFIG_NAME,
                           DEF_STORED_SEARCH_CONFIG_NAME,
                           DEF_KEY_EXPERIMENT_NAME)

def _list_run_dirs(env: Environment):
    root_dir = env.get_nested_key(['observers', 'file', 'path'])
    possible_dirs = [os.path.join(root_dir, dir_)
            for dir_ in os.listdir(root_dir) 
            if os.path.isdir(os.path.join(root_dir, dir_))]
    run_dirs = [dir_ 
                for dir_ in possible_dirs
                if os.path.isfile(os.path.join(dir_, 'run.json'))]
    return run_dirs

def _get_sacred_run_dict(run_dir):
    with open(os.path.join(run_dir, 'run.json')) as file_:
        return json.load(file_)

def _get_search_def_dict(run_dir):
    try:
        with open(os.path.join(run_dir, DEF_STORED_SEARCH_CONFIG_NAME)) as file_:
            return json.load(file_)
    except:
        return None

def _get_run_def_dict(run_dir):
    try:
        with open(os.path.join(run_dir, DEF_STORED_RUN_CONFIG_NAME)) as file_:
            return json.load(file_)
    except:
        return None

def _get_metric_dict(run_dir):
    try:
        with open(os.path.join(run_dir, 'metrics.json')) as file_:
            return json.load(file_)
    except:
        return None

class Run:
    def __init__(self, dir_):
        self.run_dir = dir_
        self.sacred_run = _get_sacred_run_dict(dir_)
        self.run_def = _get_run_def_dict(dir_)
        self.search_def = _get_search_def_dict(dir_)
        self.metric_dict = _get_metric_dict(dir_)

    def get_experiment_name(self):
        return self.run_def[DEF_KEY_EXPERIMENT_NAME]

    def get_run_start_time(self):
        return isoparse(self.sacred_run['start_time'])

    def get_search_start_time(self):
        if self.search_def is None:
            return None
        else:
            if 'execute_time' in self.search_def:
                return isoparse(self.search_def['execute_time'])
            else:
                return None

    def get_metric_value(self, metric):
        # returns last metric value
        if self.metric_dict is None:
            return None
        else:
            if metric in self.metric_dict:
                return self.metric_dict[metric]['values'][-1]
            else:
                return None

def combine_runs_into_searches(run_list):
    searches = {}
    # a search is defined by its start date format string and experiment name
    for run in run_list:
        start_time = run.get_search_start_time()
        if start_time is None:
            continue
        date_str = start_time.isoformat()
        experiment_name = run.get_experiment_name()
        search_key = (date_str, experiment_name)
        if search_key not in searches:
            searches[search_key] = []
        searches[search_key].append(run)
    return searches

def get_best_run_in_list_of_runs(run_list, metric, highest=True):
    filtered_to_those_with_metric = list(filter(
        lambda run: (run.get_metric_value(metric) is not None), run_list))
    multiplier = 1 if highest else -1
    if len(filtered_to_those_with_metric) > 0:
        return max(filtered_to_those_with_metric,
                   key=lambda run: multiplier * run.get_metric_value(metric))
    else:
        return None

def get_searches_from_env(env):
    runs = [Run(dir_)
            for dir_ in _list_run_dirs(env)]
    runs = sorted(runs, key=lambda run: run.get_run_start_time())
    searches = combine_runs_into_searches(runs)
    return searches

def sorted_searches_iterator(env):
    searches = get_searches_from_env(env)
    return sorted(searches.items(), key=lambda x: isoparse(x[0][0]))

def best(args, env):
    out_list = []
    for search, run_list in sorted_searches_iterator(env):
        best_run = get_best_run_in_list_of_runs(run_list, args.metric, highest=(not args.lowest))
        results = {
            'search_name': search[1],
            'search_time': search[0],
            'best_run': best_run
        }
        out_list.append(results)
    return out_list

def list_runs(args, env):
    search_list = []
    for search, run_list in sorted_searches_iterator(env):
        metric_table = []
        metrics_occurred = {
            metric: False for metric in args.metrics
        }
        for run in run_list:
            run_metrics = []
            for metric in args.metrics:
                metric_value = run.get_metric_value(metric)
                run_metrics.append(metric_value)
                if metric_value is not None and not metrics_occurred[metric]:
                    metrics_occurred[metric] = True
            metric_table.append([run.run_dir] + run_metrics)
        search_list.append({'table': metric_table,
                            'search_name': search[1],
                            'search_time': search[0],
                            'occurrences': sum(metrics_occurred.values())})
    return search_list

def main():
    parser = argparse.ArgumentParser(description='Execute Declair experiment definition file.')
    parser.add_argument('-e', '--env',
                        help='environment file to use (default: check default candidate files)')
    parser.add_argument('-t', '--table-format', default='simple',
                        help='what tabulate format to use for tables')
    subparsers = parser.add_subparsers(help='commands', dest='command')

    # 'best' command lists runs with best value in some metric
    best_parser = subparsers.add_parser('best')
    best_parser.add_argument('metric')
    best_parser.add_argument('-l', '--lowest', action='store_true')

    # 'list' command lists runs with some chosen metrics
    list_parser = subparsers.add_parser('list')
    list_parser.add_argument('metrics', type=str, nargs='+',)

    args = parser.parse_args()
    env = Environment.from_file(args.env) if args.env else get_path_env('.')

    if args.command == 'best':
        results_list = best(args, env)
        print("Runs which did not return {}:".format(args.metric))
        table_without_metric = [(result['search_time'], result['search_name'])
                          for result in results_list if result['best_run'] is None]
        print(
            tabulate(table_without_metric, headers=['Start time', 'Experiment name'], tablefmt=args.table_format))
        print("Runs which returned {}:".format(args.metric))
        table_with_metric = [(result['search_time'], result['search_name'],
                              result['best_run'].run_dir,
                              result['best_run'].get_metric_value(args.metric))
                      for result in results_list if result['best_run'] is not None]
        print(
            tabulate(table_with_metric, headers=['Start time', 'Experiment name',
                                                 'Run dir', 'Metric value'], tablefmt=args.table_format))

    if args.command == 'list':
        table_list = list_runs(args, env)
        print("Searches which did not return any of the metrics:")
        table_without_metric = [(result['search_time'], result['search_name'])
                          for result in table_list if result['occurrences'] == 0]
        print(
            tabulate(table_without_metric, headers=['Start time', 'Experiment name'], tablefmt=args.table_format))
        for n in range(1, len(args.metrics)):
            searches_with_n = [result
                       for result in table_list
                       if result['occurrences'] == n]
            if len(searches_with_n) > 0:
                print("Searches in which {} of the metrics occurred:".format(n))
                for result in searches_with_n:
                    print("\t{} at {}".format(result['search_time'], result['search_name']))
                    table = result['table']
                    print(
                        tabulate(table, headers=['Run directory', *args.metrics], tablefmt=args.table_format)
                    )
        searches_with_all = [result
                   for result in table_list
                   if result['occurrences'] == len(args.metrics)]
        if len(searches_with_all) > 0:
            print("Searches in which all of the metrics occurred:")
            for result in searches_with_all:
                print("\t{} at {}".format(result['search_time'], result['search_name']))
                table = result['table']
                print(
                    tabulate(table, headers=['Run directory', *args.metrics], tablefmt=args.table_format)
                )
        else:
            print("None of the searches contained all of these metrics")



if __name__ == '__main__':
    main()
